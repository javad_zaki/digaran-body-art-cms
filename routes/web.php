<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\FinancialController;
use App\Http\Controllers\Content\SliderController;
use App\Http\Controllers\Content\PromoController;
use App\Http\Controllers\Content\ShippingController;
use App\Http\Controllers\Content\TaxController;
use App\Http\Controllers\Content\SupportController;
use App\Http\Controllers\Content\AboutController;
use App\Http\Controllers\OperatorController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/users', [UserController::class, 'index'])->name('users');
        Route::get('/users/{id}', [UserController::class, 'profile'])->name('users.profile');
        Route::get('/users/{id}/enable', [UserController::class, 'enable'])->name('users.enable');
        Route::get('/users/{id}/disable', [UserController::class, 'disable'])->name('users.disable');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/operators', [OperatorController::class, 'index'])->name('operators');
        Route::get('/operators/create', [OperatorController::class, 'create'])->name('operators.create');
        Route::get('/operators/{id}/update', [OperatorController::class, 'update'])->name('operators.update');
        Route::post('/operators/store', [OperatorController::class, 'store'])->name('operators.store');
        Route::get('/operators/{id}/enable', [OperatorController::class, 'enable'])->name('operators.enable');
        Route::get('/operators/{id}/disable', [OperatorController::class, 'disable'])->name('operators.disable');
    });


    Route::group(['middleware' => ['role:super-admin']], function () {
        Route::get('/roles', [RoleController::class, 'index'])->name('roles');
        Route::get('/roles/create', [RoleController::class, 'create'])->name('roles.create');
        Route::get('/roles/{id}/update', [RoleController::class, 'update'])->name('roles.update');
        Route::post('/roles/store', [RoleController::class, 'store'])->name('roles.store');
    });

    Route::group(['middleware' => ['role:super-admin']], function () {
        Route::get('/permissions', [PermissionController::class, 'index'])->name('permissions');
        Route::get('/permissions/create', [PermissionController::class, 'create'])->name('permissions.create');
        Route::get('/permissions/{id}/update', [PermissionController::class, 'update'])->name('permissions.update');
        Route::post('/permissions/store', [PermissionController::class, 'store'])->name('permissions.store');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/orders', [OrderController::class, 'index'])->name('orders');
        Route::get('/orders/{id}', [OrderController::class, 'overview'])->name('orders.overview');
        Route::get('/orders/status/{id}/{status}', [OrderController::class, 'status'])->name('orders.status');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/products/create', [ProductController::class, 'create'])->name('products.create');
        Route::get('/products/{id}/update', [ProductController::class, 'update'])->name('products.update');
        Route::post('/products/store', [ProductController::class, 'store'])->name('products.store');
        Route::get('/products', [ProductController::class, 'index'])->name('products');
        Route::get('/products/{id}', [ProductController::class, 'overview'])->name('products.overview');
        Route::get('/products/{id}/enable', [ProductController::class, 'enable'])->name('products.enable');
        Route::get('/products/{id}/disable', [ProductController::class, 'disable'])->name('products.disable');
        Route::get('/products/{id}/featured', [ProductController::class, 'featured'])->name('products.featured');
        Route::get('/products/{id}/general', [ProductController::class, 'general'])->name('products.general');
        Route::get('/products/{id}/delete-multimedia', [ProductController::class, 'deleteMultimedia'])->name('products.delete.multimedia');
        Route::get('/products/{id}/color', [ProductController::class, 'color'])->name('products.color');
        Route::post('/products/store-color', [ProductController::class, 'storeColor'])->name('products.store.color');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/courses/create', [CourseController::class, 'create'])->name('courses.create');
        Route::get('/courses/{id}/update', [CourseController::class, 'update'])->name('courses.update');
        Route::post('/courses/store', [CourseController::class, 'store'])->name('courses.store');
        Route::get('/courses', [CourseController::class, 'index'])->name('courses');
        Route::get('/courses/{id}', [CourseController::class, 'overview'])->name('courses.overview');
        Route::get('/courses/{id}/enable', [CourseController::class, 'enable'])->name('courses.enable');
        Route::get('/courses/{id}/disable', [CourseController::class, 'disable'])->name('courses.disable');
        Route::get('/courses/{id}/featured', [CourseController::class, 'featured'])->name('courses.featured');
        Route::get('/courses/{id}/general', [CourseController::class, 'general'])->name('courses.general');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/artists/create', [ArtistController::class, 'create'])->name('artists.create');
        Route::get('/artists/{id}/update', [ArtistController::class, 'update'])->name('artists.update');
        Route::post('/artists/store', [ArtistController::class, 'store'])->name('artists.store');
        Route::get('/artists', [ArtistController::class, 'index'])->name('artists');
        Route::get('/artists/{id}', [ArtistController::class, 'profile'])->name('artists.profile');
        Route::get('/artists/{id}/enable', [ArtistController::class, 'enable'])->name('artists.enable');
        Route::get('/artists/{id}/disable', [ArtistController::class, 'disable'])->name('artists.disable');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/content/sliders/create', [SliderController::class, 'create'])->name('content.sliders.create');
        Route::get('/content/sliders/{id}/update', [SliderController::class, 'update'])->name('content.sliders.update');
        Route::post('/content/sliders/store', [SliderController::class, 'store'])->name('content.sliders.store');
        Route::get('/content/sliders', [SliderController::class, 'index'])->name('content.sliders');
        Route::get('/content/sliders/{id}/disable', [SliderController::class, 'disable'])->name('content.sliders.disable');
        Route::get('/content/promo/create', [PromoController::class, 'create'])->name('content.promos.create');
        Route::get('/content/promo/{id}/update', [PromoController::class, 'update'])->name('content.promos.update');
        Route::post('/content/promo/store', [PromoController::class, 'store'])->name('content.promos.store');
        Route::get('/content/promo', [PromoController::class, 'index'])->name('content.promos');
        Route::get('/content/sliders/{id}/enable', [SliderController::class, 'enable'])->name('content.sliders.enable');
        Route::get('/content/promo/{id}/enable', [PromoController::class, 'enable'])->name('content.promos.enable');
        Route::get('/content/promo/{id}/disable', [PromoController::class, 'disable'])->name('content.promos.disable');
        Route::get('/content/supports', [SupportController::class, 'index'])->name('content.supports');
        Route::get('/content/supports/update', [SupportController::class, 'update'])->name('content.supports.update');
        Route::get('/content/supports/create', [SupportController::class, 'create'])->name('content.supports.create');
        Route::get('/content/supports/enable', [SupportController::class, 'enable'])->name('content.supports.enable');
        Route::get('/content/supports/disable', [SupportController::class, 'disable'])->name('content.supports.disable');
        Route::post('/content/supports/store', [SupportController::class, 'store'])->name('content.supports.store');
        Route::get('/content/shipping/update', [ShippingController::class, 'update'])->name('content.shipping.update');
        Route::get('/content/shipping/create', [ShippingController::class, 'create'])->name('content.shipping.create');
        Route::get('/content/shipping/index', [ShippingController::class, 'index'])->name('content.shipping');
        Route::get('/content/shipping/enable', [ShippingController::class, 'enable'])->name('content.shipping.enable');
        Route::get('/content/shipping/disable', [ShippingController::class, 'disable'])->name('content.shipping.disable');
        Route::post('/content/shipping/store', [ShippingController::class, 'store'])->name('content.shipping.store');
        Route::get('/content/tax/index', [TaxController::class, 'index'])->name('content.tax');
        Route::get('/content/tax/update', [TaxController::class, 'update'])->name('content.tax.update');
        Route::get('/content/tax/create', [TaxController::class, 'create'])->name('content.tax.create');
        Route::get('/content/tax/enable', [TaxController::class, 'enable'])->name('content.tax.enable');
        Route::get('/content/tax/disable', [TaxController::class, 'disable'])->name('content.tax.disable');
        Route::post('/content/tax/store', [TaxController::class, 'store'])->name('content.tax.store');
        Route::get('/content/about/index', [AboutController::class, 'index'])->name('content.abouts');
        Route::get('/content/about/create', [AboutController::class, 'create'])->name('content.abouts.create');
        Route::get('/content/about/update', [AboutController::class, 'update'])->name('content.abouts.update');
        Route::post('/content/about/store', [AboutController::class, 'store'])->name('content.abouts.store');
        Route::get('/content/about/enable', [AboutController::class, 'enable'])->name('content.abouts.enable');
        Route::get('/content/about/disable', [AboutController::class, 'disable'])->name('content.abouts.disable');
        Route::get('/financial', [FinancialController::class, 'index'])->name('financial');
    });

    Route::group(['middleware' => ['role:super-admin|admin']], function () {
        Route::get('/categories', [CategoryController::class, 'index'])->name('categories');
        Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
        Route::get('/categories/{id}/update', [CategoryController::class, 'update'])->name('categories.update');
        Route::post('/categories/store', [CategoryController::class, 'store'])->name('categories.store');
        Route::get('/categories/{id}/enable', [CategoryController::class, 'enable'])->name('categories.enable');
        Route::get('/categories/{id}/disable', [CategoryController::class, 'disable'])->name('categories.disable');
    });

});

Route::get('/route-cache', function() {
    \Illuminate\Support\Facades\Artisan::call('route:cache');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    return 'cleared';
});


Route::get('cdn', function (Request $request)
{

    $file = (!empty($request->get('file')))? $request->get('file'):$request->get('amp;file');
    $path = storage_path("app/public/".$request->get('entity')."/".$file);


    if (!File::exists($path)) {
        abort(404);
    }

    $type = File::mimeType($path);
    $size = File::size($path);

    $headers = [
        'Access-Control-Expose-Headers' => 'Content-Disposition, Content-Length, X-Content-Transfer-Id',
        'Content-Type' => $type,
        'Content-Size' => $size,
    ];

    return new \Symfony\Component\HttpFoundation\BinaryFileResponse($path, 200 , $headers);
});

require __DIR__.'/auth.php';
