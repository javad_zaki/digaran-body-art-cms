@extends('layout.app')

@section('title')
    <title>پروفایل محصول</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/extensions/plyr.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/plugins/extensions/media-plyr.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-ecommerce.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">پروفایل آموزش</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/courses'}}">آموزش ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['course']['code']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-6 align-items-center justify-content-center mb-2 mb-md-0">
                                    <div class="video-player" id="plyr-video-player">
                                        <iframe src="{{$body['course']['video_link']}}?origin=https://plyr.io&iv_load_policy=3&modestbranding=1&playsinline=1&showinfo=0&rel=0&enablejsapi=1" title="{{$body['course']['title']}}" allow="autoplay;" allowfullscreen>
                                        </iframe>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p class="text-muted">{{$body['course']['category']['name']}}</p>
                                    <h5>{{$body['course']['title']}}</h5>
                                    <h6>{{$body['course']['subtitle']}}</h6>
                                    <div class="ecommerce-details-price d-flex flex-wrap mt-2">
                                        <p class="text-primary font-medium-3 mr-1 mb-0">{{gmdate("H:i:s",$body['course']['duration'])}}<sup class="font-small-3 text-dark"> مدت زمان </sup></p>
                                        <span class="pl-1 font-medium-3 border-left"></span>
                                        <p class="text-primary font-medium-3 mr-1 mb-0">{{gmdate($body['course']['artist']['name'])}}<sup class="font-small-3 text-dark"> مدرس </sup></p>
                                    </div>
                                    <hr>
                                    <p>{{$body['course']['short_description']}}</p>
                                    <hr>
                                    <p>وضعیت :
                                        @switch($body['course']['status'])
                                            @case('enable')
                                                <span class="text-success">فعال</span>
                                            @break
                                            @case('disable')
                                                <span class="text-danger">غیر فعال</span>
                                            @break
                                        @endswitch
                                    </p>
                                    <div class="d-flex flex-column flex-sm-row">
                                        <a href="{{route('courses.update', ['id' => $body['course']['id']])}}" class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">ویرایش</a>
                                        <a href="{{route('courses.enable', ['id' => $body['course']['id']])}}" class="btn btn-success mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">فعال</a>
                                        <a href="{{route('courses.disable', ['id' => $body['course']['id']])}}" class="btn btn-outline-danger waves-effect waves-light">غیر فعال</a>
                                    </div>
                                    <hr>
                                    @foreach(explode(',', $body['course']['tags']) as $item)
                                        <div class="chip mr-1">
                                            <div class="chip-body">
                                                <div class="avatar">
                                                    <i class="feather icon-tag"></i>
                                                </div>
                                                <span class="chip-text">{{$item}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/media/plyr.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/media/plyr.polyfilled.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/extensions/media-plyr.js') }}"></script>
@endsection
