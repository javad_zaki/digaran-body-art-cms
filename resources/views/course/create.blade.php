@extends('layout.app')

@section('title')
    <title>افزودن آموزش</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond-plugin-image-preview.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/select/select2.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">افزودن آموزش</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/courses'}}">آموزش ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">افزودن آموزش</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                    <section>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <form class="form" enctype="multipart/form-data" method="POST" action="{{route('courses.store')}}">
                                    @csrf
                                    <div class="col-12 col-md-8 offset-md-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="divider divider-left mb-2">
                                                    <div class="divider-text font-medium-2">فرم افزودن آموزش</div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-12">
                                                        <div class="form-body">
                                                            <div class="row">
                                                                <div class="col-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <select class="select2 form-control" name="domain" autofocus>
                                                                            <option selected>بخش</option>
                                                                            <option value="tattoo">تاتو</option>
                                                                            <option value="piercing">پیرسینگ</option>
                                                                        </select>
                                                                        {!! $errors->first('domain', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <select class="select2 form-control" name="category">
                                                                            <option selected>دسته بندی</option>
                                                                            @foreach($body['categories'] as $category)
                                                                                <option value="{{$category['id']}}">{{$category['name']}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        {!! $errors->first('category', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <div class="form-group">
                                                                        <select class="select2 form-control" name="artist">
                                                                            <option selected>مدرس</option>
                                                                            @foreach($body['artists'] as $artist)
                                                                                <option value="{{$artist['id']}}">{{$artist['name']}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        {!! $errors->first('artist', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="title" placeholder="عنوان آموزش" maxlength="300"/>
                                                                        </div>
                                                                        {!! $errors->first('title', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="subtitle" placeholder="زیر عنوان" maxlength="300"/>
                                                                        </div>
                                                                        {!! $errors->first('subtitle', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 col-md-4">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="duration" placeholder="مدت زمان (ثانیه)" maxlength="15"/>
                                                                        </div>
                                                                        {!! $errors->first('duration', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <div class="card" style="margin-bottom: 0 !important;">
                                                                        <div class="divider">
                                                                            <div class="divider-text">برچسب ها</div>
                                                                        </div>
                                                                        <div class="card-body d-flex justify-content-between align-items-start flex-column">
                                                                            <a class="btn btn-primary w-100 box-shadow-1 waves-effect waves-light text-white" onclick="addTag()">افزودن برچسب<i class="feather icon-plus"></i></a>
                                                                        </div>
                                                                        <div class="card-body d-flex justify-content-around flex-column">
                                                                            <div id="tags"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <fieldset class="form-label-group mb-0">
                                                                        <textarea data-length="5000" class="form-control char-textarea" name="video_link" rows="3" placeholder="کد نمایش ویدئو"></textarea>
                                                                        <label for="textarea-counter">کد نمایش ویدئو</label>
                                                                    </fieldset>
                                                                    <small class="counter-value float-right"><span class="char-count">0</span> / 5000 </small>
                                                                    {!! $errors->first('video_link', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <fieldset class="form-label-group mb-0">
                                                                        <textarea data-length="5000" class="form-control char-textarea" name="short_description" rows="3" placeholder="توضیح کوتاه"></textarea>
                                                                        <label for="textarea-counter">توضیح کوتاه</label>
                                                                    </fieldset>
                                                                    <small class="counter-value float-right"><span class="char-count">0</span> / 5000 </small>
                                                                    {!! $errors->first('short_description', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <input type="file" id="cover" name="cover" class="filepond"
                                                                           accept="image/png, image/jpg, image/jpeg"
                                                                           data-allow-image-preview="true"
                                                                           data-instant-upload="false">
                                                                    {!! $errors->first('cover', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                               {{-- <div class="col-12 mt-2">
                                                                    <input type="file" id="video" name="video" class="filepond"
                                                                           accept="video/mp4, video/mpeg, video/ogg, video/webm"
                                                                           data-allow-image-preview="true"
                                                                           data-instant-upload="false">
                                                                    {!! $errors->first('video', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>--}}
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                                <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('vendor-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-encode.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-type.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-size.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-exif-orientation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-preview.min.js') }}"></script>
@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
@endsection

@section('custom-js')
    <script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('js/scripts/forms/number-input.js') }}"></script>
    <script>
        function addTag(){
            $("#tags").append("<div class=\"input-group mt-2\">\n" +
                "                                                                                        <input type=\"text\" class=\"form-control\" name=\"tags[]\" placeholder=\"عنوان برچسب\" maxlength=\"300\"/>\n" +
                "                                                                                    </div>");
        }

        FilePond.registerPlugin(
            FilePondPluginFileEncode,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
        );

        const cover = FilePond.create(
            document.getElementById('cover'),
        );

        cover.setOptions({
            allowFileSizeValidation:true,
            maxFiles: 1,
            maxParallelUploads: 1,
            allowMultiple: false,
            labelIdle:'برای آپلود کردن کاور ویدئو اینجا کلیک کنید',
        });

        /*const video = FilePond.create(
            document.getElementById('video'),
        );

        video.setOptions({
            allowFileSizeValidation:true,
            maxFiles: 1,
            maxParallelUploads: 1,
            allowMultiple: false,
            labelIdle:'برای آپلود کردن ویدئوی آموزش اینجا کلیک کنید',
        });*/

    </script>
@endsection
