@extends('layout.app')

@section('title')
    <title>پروفایل مشتری</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-ecommerce.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">پروفایل مشتری</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/users'}}">مشتری ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['user']['name']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">

                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <div class="card">
                                <div class="card-header mx-auto">
                                    <div class="avatar avatar-xl">
                                        <img class="img-fluid" src="{{asset('/images/default/default.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body text-center">
                                        <h4>{{$body['user']['name']}}</h4>
                                        @switch($body['user']['status'])
                                            @case('enable')
                                            <div class="chip chip-success">
                                                <div class="chip-body">
                                                    <div class="chip-text">فعال</div>
                                                </div>
                                            </div>
                                            @break
                                            @case('disable')
                                            <div class="chip chip-danger">
                                                <div class="chip-body">
                                                    <div class="chip-text">غیر فعال</div>
                                                </div>
                                            </div>
                                            @break
                                        @endswitch
                                        <div class="card-btns d-flex justify-content-between mt-1">
                                            <a href="{{route('users.enable', ['id' => $body['user']['id']])}}" class="btn btn-outline-success waves-effect waves-light">فعال</a>
                                            <a href="{{route('users.disable', ['id' => $body['user']['id']])}}" class="btn btn-outline-danger waves-effect waves-light">غیرفعال</a>
                                        </div>
                                        <hr class="my-2">
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i> نام و نام خانوادگی
                                            </div>
                                            <div class="float-right">
                                                {{$body['user']['name']}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>شماره موبایل
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['user']['mobile'])? '__':$body['user']['mobile'])}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>شماره ملی
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['user']['national_id'])? '__':$body['user']['national_id'])}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>ایمیل
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['user']['email'])? '***':$body['user']['email'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری سفارش های مشتری</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-user-orders-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">سفارش ها</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>کل اقلام</th>
                                        <th>مجموع</th>
                                        <th>هزینه باربری</th>
                                        <th>مجموع تخفیف</th>
                                        <th>مجموع مالیات</th>
                                        <th>مجموع کل</th>
                                        <th>مبلغ قابل پرداخت</th>
                                        <th>استان</th>
                                        <th>شهر</th>
                                        <th>دسته</th>
                                        <th>تاریخ سفارش</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['orders']))
                                        @foreach($body['orders'] as $order)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$order['code']}}</td>
                                                <td>{{$order['qty']}}</td>
                                                <td>{{number_format($order['subtotal'])}}</td>
                                                <td>{{number_format($order['shipping'])}}</td>
                                                <td>{{number_format($order['discount'])}}</td>
                                                <td>{{number_format($order['tax'])}}</td>
                                                <td>{{number_format($order['total'])}}</td>
                                                <td>{{number_format($order['payable'])}}</td>
                                                <td>{{$order['province']}}</td>
                                                <td>{{$order['city']}}</td>
                                                <td>
                                                    @switch($order['domain'])

                                                        @case('tattoo')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">تاتو</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('piercing')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">پیرسینگ</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td>{{\verta($order['created_at']->toDateString())->format('Y/m/d')}}</td>
                                                <td>
                                                    @switch($order['status'])

                                                        @case('pending')
                                                        <div class="chip chip-primary">
                                                            <div class="chip-body">
                                                                <div class="chip-text">در حال بررسی</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('queued')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">در حال بسته بندی</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('sent')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">ارسال شده</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('returned')
                                                        <div class="chip chip-danger">
                                                            <div class="chip-body">
                                                                <div class="chip-text">مرجوعی</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('unpaid')
                                                        <div class="chip chip-warning">
                                                            <div class="chip-body">
                                                                <div class="chip-text">پرداخت نشده</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('orders.overview', ['id' => $order['id']])}}" data-toggle="tooltip" data-placement="top" title="جزییات سفارش"><span class="action-edit font-medium-5"><i class="feather icon-info"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>

                        {{--<div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h3>مدارک شناسایی</h3>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <hr class="my-1">
                                        <div class="row mt-1">
                                            @if(is_null($body['identities']))
                                                <div class="col-12 col-md-6">
                                                    <img class="card-img img-fluid mb-1" src="{{asset('images/default/identity-card-front.jpg')}}" alt="">
                                                </div>
                                                <div class="col-12 col-md-6">
                                                    <img class="card-img img-fluid mb-1" src="{{asset('images/default/identity-card-back.jpg')}}" alt="">
                                                </div>
                                            @else
                                                @foreach($body['identities'] as $identity)
                                                    <div class="col-12 col-md-6">
                                                        <img class="card-img img-fluid mb-1" src="{{$identity['file_name']}}" alt="">
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        <hr class="my-1 mb-4">
                                    </div>
                                </div>
                            </div>
                        </div>
--}}
                       {{-- <div class="col-12 col-md-3">
                            <div class="card">
                                <div class="card-header mx-auto">
                                    <div class="avatar avatar-xl bg-gradient-white">
                                        <img class="img-fluid" src="{{asset('/images/default/meli-logo.png')}}" alt="">
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body text-center">
                                        <h4>{{(is_null($body['bank_account'])) ? '***' : $body['bank_account']['bank_name']}}</h4>
                                        @switch($body['user']['bank_account_status'])

                                            @case('pending')
                                            <div class="chip chip-primary">
                                                <div class="chip-body">
                                                    <div class="chip-text">در حال بررسی</div>
                                                </div>
                                            </div>
                                            @break

                                            @case('approved')
                                            <div class="chip chip-success">
                                                <div class="chip-body">
                                                    <div class="chip-text">تایید شده</div>
                                                </div>
                                            </div>
                                            @break

                                            @case('disapproval')
                                            <div class="chip chip-gray">
                                                <div class="chip-body">
                                                    <div class="chip-text">نامعتبر</div>
                                                </div>
                                            </div>
                                            @break

                                        @endswitch
                                        <div class="card-btns d-flex justify-content-between">
                                            <a href="{{route('user_account_disapproval', ['user_id' => $body['user']['id']])}}" class="btn btn-outline-warning waves-effect waves-light">نامعتبر</a>
                                            <a href="{{route('user_account_approved', ['user_id' => $body['user']['id']])}}" class="btn btn-outline-success waves-effect waves-light">تایید شده</a>
                                        </div>
                                        <hr class="my-2">
                                        <div class="d-flex justify-content-between">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>نام بانک
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['bank_account'])? '***':$body['bank_account']['bank_name'])}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>شماره حساب
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['bank_account'])? '***':$body['bank_account']['account_number'])}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>شماره شبا
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['bank_account'])? '***':'IR'.$body['bank_account']['shaba_number'])}}
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="float-left">
                                                <i class="feather icon-chevron-left text-warning mr-50"></i>شماره کارت
                                            </div>
                                            <div class="float-right">
                                                {{(is_null($body['bank_account'])? '***':$body['bank_account']['card_number'])}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>--}}


               {{-- <section>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-5 text-bold-600">قرارداد</div>
                                    </div>
                                    <div class="col-12">
                                        <div class="chip chip-info font-medium-1 mr-1 mb-1">
                                            <div class="chip-body">
                                                <div class="avatar">
                                                    <span><i class="feather icon-hash"></i></span>
                                                </div>
                                                <span class="chip-text">{{(!is_null($body['contract']))? $body['contract']['issue_tracking'] : '***'}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    --}}{{--<a href="" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light"> بلوک کردن</a>
                                    <a href="" class="btn btn-outline-success mr-1 mb-1 waves-effect waves-light">آزاد کردن</a>--}}{{--
                                    <div class="card border-secondary text-center bg-transparent p-md-3">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 pl-2">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> مبلغ سپرده
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? number_format($body['contract']['investment_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> درصد درآمد سرمایه گذاری
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['interest_rates'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>درصد مالیات بیمه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['insurance_tax'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>درصد سهم معرف
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['stakeholder_share'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>درصد درآمد خالص
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? ($body['contract']['interest_rates'] - $body['contract']['insurance_tax']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>افزایش سپرده
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['increased_investment_amount'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> تاریخ شروع قرارداد
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? \verta($body['contract']['start_date']->toDateString())->format('Y/m/d') : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> درآمد سرمایه گذاری
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? number_format($body['contract']['interest_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> مالیات بیمه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? number_format($body['contract']['insurance_tax_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> سهم معرف
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? number_format(($body['contract']['net_interest_amount'] * $body['contract']['stakeholder_share'])/100) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>درآمد خالص
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? number_format($body['contract']['net_interest_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>سود واریز شده
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['deposit_number'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> کاهش سپرده
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? $body['contract']['decreased_investment_amount'] : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> تاریخ پایان قرارداد
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['contract']))? \verta($body['contract']['end_date']->toDateString())->format('Y/m/d') : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-5 text-bold-600">حساب</div>
                                    </div>
                                    <div class="chip {{(!is_null($body['account']))? ($body['account']['is_active'] == 1)? 'chip-success' : 'chip-warning' : '***'}} font-medium-1 mr-1 float-right">
                                        <div class="chip-body">
                                            <span class="chip-text">{{(!is_null($body['account']))? ($body['account']['is_active'] == 1)? 'آزاد' : 'بلوک' : '***'}}</span>
                                        </div>
                                    </div>
                                    <a href="{{route('user_account_inactive',['user_id' => $body['user']['id']])}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light"> بلوک کردن</a>
                                    <a href="{{route('user_account_active',['user_id' => $body['user']['id']])}}" class="btn btn-outline-success mr-1 mb-1 waves-effect waves-light">آزاد کردن</a>
                                    <div class="card border-secondary text-center bg-transparent p-md-3">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 pl-2">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل درآمد سرمایه گذاری
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_investment_incomes']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل مالیات بیمه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_insurance_tax']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل درآمد خالص
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_net_incomes']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل درآمد طرح ویژه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_referral_incomes']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> کل درآمد طرح طلایی
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_reward']) : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> کل برداشتی ها
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format($body['account']['total_withdrawal']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل درآمد
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['account']))? number_format(($body['account']['total_net_incomes'] + $body['account']['total_referral_incomes'] + $body['account']['total_reward']) - $body['account']['total_withdrawal']) : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-3">
                                        <div class="series-info d-flex align-items-center pt-1">
                                            <span class="text-bold-600 text-dark font-medium-3 mx-50">مانده قابل برداشت</span>
                                        </div>
                                        <div class="series-result">
                                            <p class="text-right text-success font-medium-3">
                                                {{!is_null($body['account']) ? number_format($body['account']['available_balance']) : '***'}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>--}}

                {{--<section>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-5">
                                        <div class="divider-text font-medium-5 text-bold-600">طرح طلایی و ویژه</div>
                                    </div>
                                    <div class="card border-secondary text-center bg-transparent p-md-3 mb-5">
                                        <div class="card-content pt-2 pb-2">
                                            <div class="row">
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6 pl-2">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل سرمایه گذاری
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['total_investment_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل درآمد طرح ویژه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['total_referral_incomes']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع درآمد ماهیانه طرح ویژه
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['total_referral_monthly_incomes']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کل پاداش طرح طلایی
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['total_rewards']) : '***'}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> سرمایه گذاران
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['total_users']) : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                                    <div class="d-flex justify-content-between">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i> سقف بعدی برای پاداش
                                                        </div>
                                                        <div class="float-right">
                                                            {{(!is_null($body['referral_income']))? number_format($body['referral_income']['ceiling_amount']) : '***'}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="card chat-application">
                                <div class="card-body">
                                    <div class="divider divider-left">
                                        <div class="divider-text font-medium-5 text-bold-600">سرمایه گذاران</div>
                                    </div>
                                </div>
                                <div class="chat-app-window">
                                    <div class="user-chats">
                                        <div class="chats">

                                            @if(!empty($body['referrals']))

                                                @foreach($body['referrals'] as $referral)

                                                    <div class="d-flex justify-content-start align-items-center mb-1">
                                                        <div class="avatar mr-50">
                                                            <img src="{{(is_null($referral['avatar']) ? asset('images/default/default.png') : $referral['avatar']['file_name'])}}" alt="avtar img holder" height="35" width="35">
                                                        </div>
                                                        <div class="user-page-info">
                                                            <h6 class="mb-0 text-bold-600">{{$referral['first_name'].' '.$referral['last_name']}}</h6>
                                                            <span class="font-small-2">

                                                                @switch($referral['status'])

                                                                    @case('active')
                                                                        <span class="text-success font-medium-1">فعال</span>
                                                                    @break

                                                                @endswitch

                                                            </span>
                                                        </div>
                                                        <a href="{{route('user_overview', ['user_id' => $referral['id']])}}" type="button" class="btn btn-primary btn-icon ml-auto waves-effect waves-light"><i class="feather icon-user"></i></a>
                                                    </div>

                                                @endforeach

                                            @else

                                                <div class="alert alert-light" role="alert">
                                                    <p class="mb-0">این کاربر هنوز سرمایه گذاری را معرفی نکرده است.</p>
                                                </div>

                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
--}}
                {{--<section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">برداشت از حساب (واریز به حساب بانکی)</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>مبلغ برداشت</th>
                                        <th>شماره ارجاع</th>
                                        <th>بانک مبدا</th>
                                        <th>شماره حساب مبدا</th>
                                        <th>بانک مقصد</th>
                                        <th>شماره شبا مقصد</th>
                                        <th>شماره حساب مقصد</th>
                                        <th>تاریخ درخواست</th>
                                        <th>تاریخ واریز</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['withdrawals']))
                                        @foreach($body['withdrawals'] as $withdrawal)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$withdrawal['issue_tracking']}}</td>
                                                <td>{{number_format($withdrawal['withdrawal_amount'])}}</td>
                                                <td>{{(is_null($withdrawal['reference_id'])? '***': $withdrawal['reference_id'])}}</td>
                                                <td>{{(is_null($withdrawal['origin_bank'])? '***': $withdrawal['origin_bank'])}}</td>
                                                <td>{{(is_null($withdrawal['origin_account_number'])? '***': $withdrawal['origin_account_number'])}}</td>
                                                <td>{{$withdrawal['destination_bank']}}</td>
                                                <td>{{$withdrawal['destination_shaba_number']}}</td>
                                                <td>{{$withdrawal['destination_account_number']}}</td>
                                                <td>{{\verta($withdrawal['created_at']->toDateString())->format('Y/m/d')}}</td>
                                                <td>{{(is_null($withdrawal['deposit_date'])? '***': \verta($withdrawal['deposit_date'])->format('Y/m/d'))}}</td>
                                                <td>
                                                    @switch($withdrawal['status'])

                                                        @case('pending')
                                                        <div class="chip chip-primary">
                                                            <div class="chip-body">
                                                                <div class="chip-text">در حال بررسی</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('done')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">موفق</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('failed')
                                                        <div class="chip chip-gray">
                                                            <div class="chip-body">
                                                                <div class="chip-text">ناموفق</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('withdrawal_show', ['withdrawal_id' => $withdrawal['id']])}}" data-toggle="tooltip" data-placement="top" title="واریز"><span class="action-edit font-medium-5"><i class="feather icon-credit-card"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>--}}

                {{--<section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">تیکت ها</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>عنوان</th>
                                        <th>تاریخ ایجاد</th>
                                        <th>تاریخ آخرین به روز رسانی</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['tickets']))
                                        @foreach($body['tickets'] as $ticket)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$ticket['issue_tracking']}}</td>
                                                <td>{{$ticket['title']}}</td>
                                                <td>{{\verta($ticket['created_at']->toDateString())->format('Y/m/d')}}</td>
                                                <td>{{\verta($ticket['updated_at']->toDateString())->format('Y/m/d')}}</td>
                                                <td>
                                                    @switch($ticket['status'])

                                                        @case('open')
                                                        <div class="chip chip-primary">
                                                            <div class="chip-body">
                                                                <div class="chip-text">باز</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('closed')
                                                        <div class="chip chip-gray">
                                                            <div class="chip-body">
                                                                <div class="chip-text">بسته</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('answered')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">پاسخ داده شده</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('ticket_show', ['issue_tracking' => $ticket['issue_tracking']])}}" data-toggle="tooltip" data-placement="top" title="پاسخ دادن"><span class="action-edit font-medium-5"><i class="feather icon-message-square"></i></span></a>
                                                    <a href="{{route('ticket_close', ['issue_tracking' => $ticket['issue_tracking']])}}" data-toggle="tooltip" data-placement="top" title="بستن تیکت"><span class="action-edit font-medium-5"><i class="feather icon-x-square"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>--}}

               {{-- <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">لیست واریز درآمد سرمایه گذاری</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>درآمد سرمایه گذاری</th>
                                        <th>مالیات بیمه</th>
                                        <th>درآمد خالص</th>
                                        <th>درآمد واریزی</th>
                                        <th>تاریخ ایجاد سند</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['interest_deposits']))
                                        @foreach($body['interest_deposits'] as $interest_deposit)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$interest_deposit['issue_tracking']}}</td>
                                                <td>{{number_format($interest_deposit['interest_amount'])}}</td>
                                                <td>{{number_format($interest_deposit['insurance_tax_amount'])}}</td>
                                                <td>{{number_format($interest_deposit['net_interest_amount'])}}</td>
                                                <td>{{number_format($interest_deposit['deposited_amount'])}}</td>
                                                <td>{{\verta($interest_deposit['created_at']->toDateString())->format('Y/m/d')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>--}}

               {{-- <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">لیست واریز درآمد طرح ویژه</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>سرمایه گذار</th>
                                        <th>کل مبلغ سرمایه گذاری</th>
                                        <th>درآمد خالص سرمایه گذار</th>
                                        <th>درآمد ماهیانه معرف</th>
                                        <th>درآمد واریز شده</th>
                                        <th>تاریخ واریز</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['referral_deposits']))
                                        @foreach($body['referral_deposits'] as $referral_deposit)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$referral_deposit['issue_tracking']}}</td>
                                                <td>{{$referral_deposit['referral']['first_name'] . ' '. $referral_deposit['referral']['last_name']}}</td>
                                                <td>{{number_format($referral_deposit['contract']['investment_amount'])}}</td>
                                                <td>{{number_format($referral_deposit['contract']['net_interest_amount'])}}</td>
                                                <td>{{number_format($referral_deposit['monthly_incomes_amount'])}}</td>
                                                <td>{{number_format($referral_deposit['deposited_amount'])}}</td>
                                                <td>{{\verta($referral_deposit['created_at']->toDateString())->format('Y/m/d')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>--}}

                {{--<section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">لیست واریز درآمد طرح طلایی</div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد پیگیری</th>
                                        <th>کل مبلغ سرمایه گذاری</th>
                                        <th>پاداش</th>
                                        <th>مبلغ واریزی</th>
                                        <th>وضعیت واریز</th>
                                        <th>تاریخ واریز</th>
                                        <th>تاریخ ایجاد سند</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['reward_deposits']))
                                        @foreach($body['reward_deposits'] as $reward_deposit)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$reward_deposit['issue_tracking']}}</td>
                                                <td>{{number_format($reward_deposit['total_investment_amount'])}}</td>
                                                <td>{{number_format($reward_deposit['reward_amount'])}}</td>
                                                <td>{{number_format($reward_deposit['deposited_amount'])}}</td>
                                                <td>{{($reward_deposit['is_deposited'] == 1) ? 'واریز شده' : 'واریز نشده'}}</td>
                                                <td>{{\verta($reward_deposit['deposit_date'])->format('Y/m/d')}}</td>
                                                <td>{{\verta($reward_deposit['created_at']->toDateString())->format('Y/m/d')}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>--}}

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/charts/chart.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")

    <script>

        $(window).on("load", function () {

            Chart.defaults.global.defaultFontFamily = "IRANSans_num";
            var $primary = '#7367F0';
            var $success = '#28C76F';
            var $danger = '#EA5455';
            var $warning = '#FF9F43';
            var $info = '#2D91FF';
            var $label_color = '#1E1E1E';
            var grid_line_color = '#dae1e7';
            var scatter_grid_color = '#f3f3f3';
            var $scatter_point_light = '#D1D4DB';
            var $white = '#fff';
            var $black = '#000';
            var themeColors = [$primary, $success, $danger, $warning, $label_color];

            // Line Chart Options
            var lineChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: grid_line_color,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            color: grid_line_color,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'نمودار خطی سال جاری'
                }
            };


            var annual_user_orders_line_chart = $("#annual-user-orders-line-chart");
            var annual_user_orders_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "سفارش ها",
                    data: {{$body['annual_user_orders_chart_data']}},
                    borderColor: $primary,
                    fill: true
                }]
            };
            var annual_user_orders_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_user_orders_line_chart_data
            };
            var annual_user_orders_line_chart_obj = new Chart(annual_user_orders_line_chart, annual_user_orders_line_chart_config);

        });

    </script>

    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/pages/app-chat.js') }}"></script>
@endsection
