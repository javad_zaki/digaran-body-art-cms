@extends('layout.app')

@section('title')
    <title>پروفایل محصول</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-ecommerce.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">پروفایل هنرمند</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/artists'}}">هنرمندان</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['artist']['name']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img src="{{(!is_null($body['artist']['multimedia']))? $body['artist']['multimedia']['primary_file_name'] : asset('/images/default/default.png') }}" class="img-fluid" alt="تصویر هنرمند">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p class="text-muted">{{$body['artist']['name']}}</p>
                                    <div class="ecommerce-details-price d-flex flex-wrap mt-2">
                                        <p class="text-primary font-medium-3 mr-1 mb-0">
                                        @switch($body['artist']['role'])

                                            @case('author')
                                                مدرس
                                            @break

                                            @case('artist')
                                                هنرمند
                                            @break

                                        @endswitch
                                            <sup class="font-small-3 text-dark"> نقش </sup>
                                        </p>
                                    </div>
                                    <hr>
                                    <p>{{$body['artist']['biography']}}</p>
                                    <hr>
                                    @foreach(json_decode($body['artist']['social'], true) as $item)
                                        <a href="{{$item['username']}}" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-{{$item['channel']}}"></i></a>
                                    @endforeach
                                    <hr>
                                    <p>وضعیت :
                                        @switch($body['artist']['status'])
                                            @case('enable')
                                                <span class="text-success">فعال</span>
                                            @break
                                            @case('disable')
                                                <span class="text-danger">غیر فعال</span>
                                            @break
                                        @endswitch
                                    </p>
                                    <div class="d-flex flex-column flex-sm-row">
                                        <a href="{{route('artists.update', ['id' => $body['artist']['id']])}}" class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">ویرایش</a>
                                        <a href="{{route('artists.enable', ['id' => $body['artist']['id']])}}" class="btn btn-success mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">فعال</a>
                                        <a href="{{route('artists.disable', ['id' => $body['artist']['id']])}}" class="btn btn-outline-danger waves-effect waves-light">غیر فعال</a>
                                    </div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/pages/app-chat.js') }}"></script>
@endsection
