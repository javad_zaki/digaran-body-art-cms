@extends('layout.app')

@section('title')
    <title>هنرمندان ها</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">هنرمندان</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">داشبورد</a>
                                    </li>
                                    <li class="breadcrumb-item active">هنرمندان</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                    <section>
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <a href="{{--{{route('contract_index')}}--}}">
                                    <div class="card text-center bg-gradient-dark">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                    <div class="avatar-content">
                                                        <i class="feather icon-users text-white font-medium-5"></i>
                                                    </div>
                                                </div>
                                                <h2 class="text-bold-700 text-white">{{$body['total_artists_count']}}</h2>
                                                <p class="mb-0 line-ellipsis">کل هنرمندان</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="{{--{{route('contract_index')}}--}}">
                                    <div class="card text-center bg-gradient-dark">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                    <div class="avatar-content">
                                                        <i class="feather icon-user-check text-white font-medium-5"></i>
                                                    </div>
                                                </div>
                                                <h2 class="text-bold-700 text-white">{{$body['total_artists_author_count']}}</h2>
                                                <p class="mb-0 line-ellipsis">کل هنرمندان مدرس</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-4">
                                <a href="{{--{{route('contract_index')}}--}}">
                                    <div class="card text-center bg-gradient-dark">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                    <div class="avatar-content">
                                                        <i class="feather icon-user text-white font-medium-5"></i>
                                                    </div>
                                                </div>
                                                <h2 class="text-bold-700 text-white">{{$body['total_artists_artist_count']}}</h2>
                                                <p class="mb-0 line-ellipsis">کل هنرمندان غیر مدرس</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </section>



                    <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">هنرمندان</div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="actions action-btns">
                                <div class="dt-buttons btn-group">
                                    <a href="{{route('artists.create')}}">
                                        <button class="btn btn-primary">
                                            <span><i class="feather icon-plus"></i>افزودن هنرمند</span>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>نام</th>
                                        <th>بیوگرافی</th>
                                        <th>نقش</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['artists']))
                                        @foreach($body['artists'] as $artist)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$artist['name']}}</td>
                                                <td>{{$artist['biography']}}</td>
                                                <td>
                                                    @switch($artist['role'])

                                                        @case('author')
                                                        <div class="chip chip-primary">
                                                            <div class="chip-body">
                                                                <div class="chip-text">مدرس</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('artist')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">هنرمند</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td>
                                                    @switch($artist['status'])

                                                        @case('enable')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('disable')
                                                        <div class="chip chip-danger">
                                                            <div class="chip-body">
                                                                <div class="chip-text">غیر فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('artists.profile', ['id' => $artist['id']])}}" data-toggle="tooltip" data-placement="top" title="پروفایل هنرمند"><span class="action-edit font-medium-5"><i class="feather icon-info"></i></span></a>
                                                    <a href="{{route('artists.enable', ['id' => $artist['id']])}}" data-toggle="tooltip" data-placement="top" title="فعال کردن"><span class="action-edit font-medium-5"><i class="feather icon-eye"></i></span></a>
                                                    <a href="{{route('artists.disable', ['id' => $artist['id']])}}" data-toggle="tooltip" data-placement="top" title="غیر فعال کردن"><span class="action-edit font-medium-5"><i class="feather icon-eye-off"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
@endsection
