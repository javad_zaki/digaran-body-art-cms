@extends('layout.app')

@section('title')
    <title>ویرایش هنرمند</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond-plugin-image-preview.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond-plugin-image-edit.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/select/select2.min.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">ویرایش هنرمند</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/artists'}}">هنرمندان</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['artist']['name']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                    <section>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <form class="form" enctype="multipart/form-data" method="POST" action="{{route('artists.store')}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$body['artist']['id']}}">
                                    <div class="col-12 col-md-8 offset-md-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="divider divider-left mb-2">
                                                    <div class="divider-text font-medium-2">فرم ویرایش هنرمند</div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-12">
                                                        <div class="form-body">

                                                            <div class="row">
                                                                <div class="col-12 col-md-6">
                                                                    <div class="form-group">
                                                                        <select class="select2 form-control" name="domain" autofocus>
                                                                            <option value="tattoo" {{($body['artist']['domain'] == 'tattoo') ? 'selected': ''}}>تاتو</option>
                                                                            <option value="piercing" {{($body['artist']['domain'] == 'piercing') ? 'selected': ''}}>پیرسینگ</option>
                                                                        </select>
                                                                        {!! $errors->first('domain', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-md-6">
                                                                    <div class="form-group">
                                                                        <select class="select2 form-control" name="role">
                                                                            <option selected>نقش</option>
                                                                            <option value="artist" {{($body['artist']['role'] == 'artist') ? 'selected': ''}}>هنرمند</option>
                                                                            <option value="author" {{($body['artist']['role'] == 'author') ? 'selected': ''}}>مدرس</option>
                                                                        </select>
                                                                        {!! $errors->first('role', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="name" placeholder="نام هنرمند" value="{{$body['artist']['name']}}" maxlength="300"/>
                                                                        </div>
                                                                        {!! $errors->first('name', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <div class="card" style="margin-bottom: 0 !important;">
                                                                        <div class="card-body d-flex justify-content-between align-items-start flex-column">
                                                                            <a class="btn btn-primary w-100 box-shadow-1 waves-effect waves-light text-white" onclick="addSocialChannel()">افزودن کانال<i class="feather icon-plus"></i></a>
                                                                        </div>
                                                                        <div class="divider">
                                                                            <div class="divider-text">شبکه های اجتماعی</div>
                                                                        </div>
                                                                        <div class="card-body d-flex justify-content-around flex-column">
                                                                            <div id="channels">
                                                                                @foreach(json_decode($body['artist']['social'], true) as $item)
                                                                                    <div class="input-group mt-2">
                                                                                        <select class="select2 form-control" name="channel_type[]">
                                                                                            <option value="instagram" {{($item['channel'] == 'instagram') ? 'selected': ''}}>اینستاگرام</option>
                                                                                            <option value="facebook" {{($item['channel'] == 'facebook') ? 'selected': ''}}>فیسبوک</option>
                                                                                            <option value="youtube" {{($item['channel'] == 'youtube') ? 'selected': ''}}>یوتیوب</option>
                                                                                        </select>
                                                                                        <input type="text" class="form-control" name="channel_username[]" placeholder="نام کاربری شبکه اجتماعی" value="{{$item['username']}}" maxlength="300"/>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <fieldset class="form-label-group mb-0">
                                                                        <textarea data-length="1000" class="form-control char-textarea" name="biography" rows="2" placeholder="بیوگرافی">{{$body['artist']['biography']}}</textarea>
                                                                        <label for="textarea-counter">بیوگرافی</label>
                                                                    </fieldset>
                                                                    <small class="counter-value float-right"><span class="char-count">0</span> / 1000 </small>
                                                                    {!! $errors->first('biography', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                                @if(!is_null($body['artist']['multimedia']))
                                                                <div class="col-12 col-lg-4 offset-lg-4">
                                                                    <img class="card-img img-fluid" src="{!! html_entity_decode(e($body['artist']['multimedia']['thumbnail_file_name'])) !!}" alt="">
                                                                </div>
                                                                @endif
                                                                <div class="col-12 mt-2">
                                                                    <input type="file" id="picture"
                                                                           name="picture" class="filepond"
                                                                           accept="image/png, image/jpg, image/jpeg"
                                                                           data-allow-image-preview="true"
                                                                           data-instant-upload="false">
                                                                    {!! $errors->first('picture', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                                <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('vendor-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-encode.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-type.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-size.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-exif-orientation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-preview.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-crop.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-resize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-transform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-edit.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/doka.js') }}"></script>
@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection

@section('custom-js')
    <script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>
    <script>

        function addSocialChannel(){
            $("#channels").append("<div class=\"input-group mt-2\">\n" +
                "                                                                                        <select class=\"select2 form-control\" name=\"channel_type[]\">\n" +
                "                                                                                            <option value=\"instagram\">اینستاگرام</option>\n" +
                "                                                                                            <option value=\"facebook\">فیسبوک</option>\n" +
                "                                                                                            <option value=\"youtube\">یوتیوب</option>\n" +
                "                                                                                        </select>\n" +
                "                                                                                        <input type=\"text\" class=\"form-control\" name=\"channel_username[]\" placeholder=\"نام کاربری شبکه اجتماعی\" maxlength=\"300\"/>\n" +
                "                                                                                    </div>");
        }

        FilePond.registerPlugin(
            FilePondPluginFileEncode,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
        );

        const picture = FilePond.create(
            document.getElementById('picture'),
        );

        picture.setOptions({
            allowFileSizeValidation:true,
            /*maxFileSize: '300KB',*/
            labelMaxFileSizeExceeded:'حجم فایل بیش از حد زیاد است',
            labelMaxFileSize: 'حداکثر اندازه فایل 100 کیلوبایت است',
            labelMaxTotalFileSizeExceeded:'اندازه کل فایل ها فراتر رفت',
            labelMaxTotalFileSize:'حداکثر اندازه کل فایل ها 100 کیلوبایت است',
            maxFiles: 1,
            maxParallelUploads: 1,
            allowMultiple: false,
            labelIdle:'برای آپلود کردن تصویر هنرمند خود اینجا کلیک کنید',
        });
    </script>
@endsection
