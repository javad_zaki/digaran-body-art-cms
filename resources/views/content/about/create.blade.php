@extends('layout.app')

@section('title')
    <title>ایجاد تنظیمات درباره ما</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">ایجاد تنظیمات درباره ما</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{route('content.abouts')}}">پروفایل های درباره ما</a>
                                    </li>
                                    <li class="breadcrumb-item active">ایجاد تنظیمات درباره ما</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <form class="form" method="POST" action="{{route('content.abouts.store')}}">
                                @csrf
                                <div class="col-12 col-md-8 offset-md-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="divider divider-left mb-2">
                                                <div class="divider-text font-medium-2">فرم ایجاد تنظیمات درباره ما</div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <fieldset class="form-label-group mb-0">
                                                                    <textarea data-length="10000" class="form-control char-textarea" name="description" rows="5" placeholder="متن درباره ما"></textarea>
                                                                    <label for="textarea-counter">متن درباره ما</label>
                                                                </fieldset>
                                                                <small class="counter-value float-right"><span class="char-count">0</span> / 10000 </small>
                                                                {!! $errors->first('description', '<span class="text-warning font-small-2">:message</span>') !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                            <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
