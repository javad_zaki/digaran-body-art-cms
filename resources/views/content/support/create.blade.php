@extends('layout.app')

@section('title')
    <title>ایجاد تنظیمات پشتیبانی</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">ایجاد تنظیمات پشتیبانی</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{route('content.supports')}}">پروفایل های پشتیبانی</a>
                                    </li>
                                    <li class="breadcrumb-item active">ایجاد تنظیمات پشتیبانی</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <form class="form" method="POST" action="{{route('content.supports.store')}}">
                                @csrf
                                <div class="col-12 col-md-8 offset-md-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="divider divider-left mb-2">
                                                <div class="divider-text font-medium-2">فرم ایجاد تنظیمات پشتیبانی</div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-12 col-md-4">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="support_number" placeholder="شماره پشتیبانی" maxlength="11" />
                                                                    </div>
                                                                    {!! $errors->first('support_number', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-12 col-md-4">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="support_email" placeholder="ایمیل پشتیبانی" maxlength="150" />
                                                                    </div>
                                                                    {!! $errors->first('support_email', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-12 col-md-4">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="availability" placeholder="زمان دسترسی" maxlength="150" />
                                                                    </div>
                                                                    {!! $errors->first('availability', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-12 mt-2">
                                                                <div class="card" style="margin-bottom: 0 !important;">
                                                                    <div class="card-body d-flex justify-content-between align-items-start flex-column">
                                                                        <a class="btn btn-primary w-100 box-shadow-1 waves-effect waves-light text-white" onclick="addSocialChannel()">افزودن کانال<i class="feather icon-plus"></i></a>
                                                                    </div>
                                                                    <div class="divider">
                                                                        <div class="divider-text">شبکه های اجتماعی</div>
                                                                    </div>
                                                                    <div class="card-body d-flex justify-content-around flex-column">
                                                                        <div id="channels">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12 mt-2">
                                                                <div class="card" style="margin-bottom: 0 !important;">
                                                                    <div class="card-body d-flex justify-content-between align-items-start flex-column">
                                                                        <a class="btn btn-primary w-100 box-shadow-1 waves-effect waves-light text-white" onclick="addfaq()">افزودن سوال<i class="feather icon-plus"></i></a>
                                                                    </div>
                                                                    <div class="divider">
                                                                        <div class="divider-text">سوال های متداول</div>
                                                                    </div>
                                                                    <div class="card-body d-flex justify-content-around flex-column">
                                                                        <div id="faq">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                            <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('custom-js')
    <script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>
    <script>
        function addSocialChannel(){
            $("#channels").append("<div class=\"input-group mt-2\">\n" +
                "                                                                                        <select class=\"select2 form-control\" name=\"channel_type[]\">\n" +
                "                                                                                            <option value=\"instagram\">اینستاگرام</option>\n" +
                "                                                                                            <option value=\"facebook\">فیسبوک</option>\n" +
                "                                                                                            <option value=\"youtube\">یوتیوب</option>\n" +
                "                                                                                            <option value=\"whatsapp\">واتس اپ</option>\n" +
                "                                                                                            <option value=\"telegram\">تلگرام</option>\n" +
                "                                                                                            <option value=\"pinterest\">پینترست</option>\n" +
                "                                                                                        </select>\n" +
                "                                                                                        <input type=\"text\" class=\"form-control\" name=\"channel_username[]\" placeholder=\"نام کاربری شبکه اجتماعی\" maxlength=\"300\"/>\n" +
                "                                                                                    </div>");
        }

        function addfaq(){
            $("#faq").append("<div class=\"input-group mt-2\">\n" +
                "                                                                                    <input type=\"text\" class=\"form-control\" name=\"questions[]\" placeholder=\"سوال\" maxlength=\"300\"/>\n" +
                "                                                                                </div>\n" +
                "                                                                                <fieldset class=\"form-label-group mt-2\">\n" +
                "                                                                                    <textarea data-length=\"2000\" class=\"form-control char-textarea\" name=\"answers[]\" rows=\"3\" placeholder=\"پاسخ\"></textarea>\n" +
                "                                                                                    <label for=\"textarea-counter\">پاسخ</label>\n" +
                "                                                                                </fieldset>\n" +
                "                                                                                <small class=\"counter-value float-right\"><span class=\"char-count\">0</span> / 2000 </small>");
        }
    </script>
@endsection
