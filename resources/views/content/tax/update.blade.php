@extends('layout.app')

@section('title')
    <title>ویرایش تنظیمات مالیات</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">ویرایش تنظیمات مالیات</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{route('content.tax')}}">پروفایل های مالیات</a>
                                    </li>
                                    <li class="breadcrumb-item active">ویرایش تنظیمات مالیات</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <form class="form" method="POST" action="{{route('content.tax.store')}}">
                                @csrf
                                <input type="hidden" name="id" value="{{$body['tax']['id']}}">
                                <div class="col-12 col-md-8 offset-md-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="divider divider-left mb-2">
                                                <div class="divider-text font-medium-2">فرم ویرایش تنظیمات مالیات</div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-12 col-md-6">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="tax" placeholder="درصد مالیات" maxlength="3" value="{{json_decode($body['tax']['detail'], true)['tax']}}"/>
                                                                    </div>
                                                                    {!! $errors->first('tax', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                            <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
