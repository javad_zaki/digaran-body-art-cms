@extends('layout.app')

@section('title')
    <title>پروفایل های مالیات</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">پروفایل های مالیات</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">داشبورد</a>
                                    </li>
                                    <li class="breadcrumb-item active">پروفایل های مالیات</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">پروفایل های مالیات</div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="actions action-btns">
                                <div class="dt-buttons btn-group">
                                    <a href="{{route('content.tax.create')}}">
                                        <button class="btn btn-primary">
                                            <span><i class="feather icon-plus"></i>افزودن پروفایل مالیات</span>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>مالیت (درصد)</th>
                                        <th>وضعیت</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['taxs']))
                                        @foreach($body['taxs'] as $tax)
                                            <tr>
                                                <td>#</td>
                                                <td>{{json_decode($tax['detail'], true)['tax']}}</td>
                                                <td>
                                                    @switch($tax['status'])

                                                        @case('enable')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('disable')
                                                        <div class="chip chip-danger">
                                                            <div class="chip-body">
                                                                <div class="chip-text">غیر فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('content.tax.update', ['id' => $tax['id']])}}" data-toggle="tooltip" data-placement="top" title="بروزرسانی"><span class="action-edit font-medium-5"><i class="feather icon-edit"></i></span></a>
                                                    <a href="{{route('content.tax.enable', ['id' => $tax['id']])}}" data-toggle="tooltip" data-placement="top" title="فعال"><span class="action-edit font-medium-5"><i class="feather icon-eye"></i></span></a>
                                                    <a href="{{route('content.tax.disable', ['id' => $tax['id']])}}" data-toggle="tooltip" data-placement="top" title="غیر فعال"><span class="action-edit font-medium-5"><i class="feather icon-eye-off"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
@endsection
