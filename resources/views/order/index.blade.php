@extends('layout.app')

@section('title')
    <title>مشتری ها</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">سفارش ها</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">داشبورد</a>
                                    </li>
                                    <li class="breadcrumb-item active">سفارش ها</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif


                <section>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-shopping-bag text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['pending_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های جدید</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-shopping-bag text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-package text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['queued_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های در حال بسته بندی</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-truck text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['sent_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های ارسال شده</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('financial_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-refresh-cw text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['returned_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis text-white">سفارش های مرجوعی</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('financial_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-credit-card text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['unpaid_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis text-white">سفارش های پرداخت نشده</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                    <section id="data-list-view" class="data-list-view-header">
                        <div class="card">

                            <div class="card-body">
                                <div class="divider divider-left mb-2">
                                    <div class="divider-text font-medium-3 text-bold-600">سفارش ها</div>
                                </div>
                            </div>

                            <div class="card-content">

                                <!-- DataTable starts -->
                                <div class="table-responsive">
                                    <table class="table data-list-view">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>کد پیگیری</th>
                                            <th>کل اقلام</th>
                                            <th>مجموع</th>
                                            <th>هزینه باربری</th>
                                            <th>مجموع تخفیف</th>
                                            <th>مجموع مالیات</th>
                                            <th>مجموع کل</th>
                                            <th>مبلغ قابل پرداخت</th>
                                            <th>استان</th>
                                            <th>شهر</th>
                                            <th>دسته</th>
                                            <th>تاریخ سفارش</th>
                                            <th>وضعیت</th>
                                            <th>عملیات</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($body['orders']))
                                            @foreach($body['orders'] as $order)
                                                <tr>
                                                    <td>#</td>
                                                    <td>{{$order['code']}}</td>
                                                    <td>{{$order['qty']}}</td>
                                                    <td>{{number_format($order['subtotal'])}}</td>
                                                    <td>{{number_format($order['shipping'])}}</td>
                                                    <td>{{number_format($order['discount'])}}</td>
                                                    <td>{{number_format($order['tax'])}}</td>
                                                    <td>{{number_format($order['total'])}}</td>
                                                    <td>{{number_format($order['payable'])}}</td>
                                                    <td>{{$order['province']}}</td>
                                                    <td>{{$order['city']}}</td>
                                                    <td>
                                                        @switch($order['domain'])

                                                            @case('tattoo')
                                                            <div class="chip chip-info">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">تاتو</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                            @case('piercing')
                                                            <div class="chip chip-info">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">پیرسینگ</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                        @endswitch
                                                    </td>
                                                    <td>{{\verta($order['created_at']->toDateString())->format('Y/m/d')}}</td>
                                                    <td>
                                                        @switch($order['status'])

                                                            @case('pending')
                                                            <div class="chip chip-primary">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">در حال بررسی</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                            @case('queued')
                                                            <div class="chip chip-info">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">در حال بسته بندی</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                            @case('sent')
                                                            <div class="chip chip-success">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">ارسال شده</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                            @case('returned')
                                                            <div class="chip chip-danger">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">مرجوعی</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                            @case('unpaid')
                                                            <div class="chip chip-warning">
                                                                <div class="chip-body">
                                                                    <div class="chip-text">پرداخت نشده</div>
                                                                </div>
                                                            </div>
                                                            @break

                                                        @endswitch
                                                    </td>
                                                    <td class="product-action">
                                                        <a href="{{route('orders.overview', ['id' => $order['id']])}}" data-toggle="tooltip" data-placement="top" title="جزییات سفارش"><span class="action-edit font-medium-5"><i class="feather icon-info"></i></span></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                                <!-- DataTable ends -->
                            </div>
                        </div>
                    </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
@endsection
