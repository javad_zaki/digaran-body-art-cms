@extends('layout.app')

@section('title')
    <title>جزییات سفارش</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-ecommerce.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">جزییات سفارش</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/orders'}}">سفارش ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['order']['code']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-5 text-bold-600">سفارش</div>
                                    </div>
                                    <a href="{{route('orders.status', ['id' => $body['order']['id'], 'status' => 'queued'])}}" class="btn btn-outline-info mr-1 mb-1 waves-effect waves-light">در حال بسته بندی</a>
                                    <a href="{{route('orders.status', ['id' => $body['order']['id'], 'status' => 'sent'])}}" class="btn btn-outline-success mr-1 mb-1 waves-effect waves-light">ارسال شد</a>
                                    <a href="{{route('orders.status', ['id' => $body['order']['id'], 'status' => 'returned'])}}" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">مرجوع شد</a>
                                    <div class="card border-secondary text-center bg-transparent p-1">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col-12 pl-2">
                                                    <div class="d-flex justify-content-between font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کد پیگیری
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['code']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>تعداد کل اقلام
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">عدد</sup>
                                                            {{$body['order']['qty']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">ریال</sup>
                                                            {{number_format($body['order']['subtotal'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>هزینه باربری
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">ریال</sup>
                                                            {{number_format($body['order']['shipping'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع مالیات
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">ریال</sup>
                                                            {{number_format($body['order']['shipping'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع تخفیف
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">ریال</sup>
                                                            {{number_format($body['order']['discount'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع کل
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">ریال</sup>
                                                            {{number_format($body['order']['total'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>تاریخ سفارش
                                                        </div>
                                                        <div class="float-right">
                                                            {{\verta($body['order']['created_at']->toDateString())->format('Y/m/d')}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>وضعیت سفارش
                                                        </div>
                                                        <div class="float-right">
                                                            @switch($body['order']['status'])

                                                                @case('pending')
                                                                <div class="chip chip-primary">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">در حال بررسی</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                                @case('queued')
                                                                <div class="chip chip-info">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">در حال بسته بندی</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                                @case('sent')
                                                                <div class="chip chip-success">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">ارسال شده</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                                @case('returned')
                                                                <div class="chip chip-danger">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">مرجوعی</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                                @case('unpaid')
                                                                <div class="chip chip-warning">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">پرداخت نشده</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                            @endswitch
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-3">
                                        <div class="series-info d-flex align-items-center pt-1">
                                            <span class="text-bold-600 text-dark font-medium-1 mx-50">مبلغ قابل پرداخت</span>
                                        </div>
                                        <div class="series-result pt-1">
                                            <p class="text-right text-success font-medium-1">
                                                <sup class="font-medium-1">ریال</sup>
                                                {{number_format($body['order']['payable'])}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-5 text-bold-600">باربری و تراکنش</div>
                                    </div>
                                    <a href="" class="btn btn-outline-success mr-1 mb-1 waves-effect waves-light">موفق</a>
                                    <a href="" class="btn btn-outline-danger mr-1 mb-1 waves-effect waves-light">ناموفق</a>
                                    <div class="card border-secondary text-center bg-transparent p-1">
                                        <div class="card-content">
                                            <div class="row">
                                                <div class="col-12 pl-2">
                                                    <div class="d-flex justify-content-between font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>استان
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['province']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>شهر
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['city']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>کد پستی
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['postal_code']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>آدرس
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['address']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>فاکتور کاغذی
                                                        </div>
                                                        <div class="float-right">
                                                            @if($body['order']['send_paper_invoice'])
                                                                ارسال شود
                                                            @else
                                                                نیازی نیست
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>شماره مرجع
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['transaction']['ref_num']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>شماره پیگیری
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['transaction']['trace_num']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>تاریخ تراکنش
                                                        </div>
                                                        <div class="float-right">
                                                            {{\verta($body['order']['transaction']['created_at']->toDateString())->format('Y/m/d')}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>وضعیت تراکنش
                                                        </div>
                                                        <div class="float-right">
                                                            @switch($body['order']['transaction']['state'])

                                                                @case('ok')
                                                                <div class="chip chip-success">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">موفق</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                                @case('canceled')
                                                                <div class="chip chip-danger">
                                                                    <div class="chip-body">
                                                                        <div class="chip-text">نا موفق</div>
                                                                    </div>
                                                                </div>
                                                                @break

                                                            @endswitch
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-3">
                                        <div class="series-info d-flex align-items-center pt-1">
                                            <span class="text-bold-600 text-dark font-medium-1 mx-50">مبلغ تراکنش</span>
                                        </div>
                                        <div class="series-result pt-1">
                                            <p class="text-right text-success font-medium-1">
                                                <sup class="font-medium-1">ریال</sup>
                                                {{number_format($body['order']['transaction']['transaction_amount'])}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-5 text-bold-600">فاکتور</div>
                                    </div>
                                    <a onclick="printDiv()" class="btn btn-outline-primary mr-1 mb-1 waves-effect waves-light">چاپ</a>
                                    <div class="card border-secondary text-center bg-transparent p-1">
                                        <div class="card-content" id="invoice">
                                            <div class="row">
                                                <div class="col-12 pl-2">
                                                    <div class="d-flex justify-content-between font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مشخصات گیرنده
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['user']['name']}} ({{$body['user']['national_id']}})
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>آدرس گیرنده
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['province']}} - {{$body['order']['city']}} - {{$body['order']['address']}} - ({{$body['order']['postal_code']}})
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>تعداد اقلام
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-1">عدد</sup>
                                                            {{$body['order']['qty']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-2">ریال</sup>
                                                            {{number_format($body['order']['subtotal'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>هزینه باربری
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-2">ریال</sup>
                                                            {{number_format($body['order']['shipping'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع مالیات
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-2">ریال</sup>
                                                            {{number_format($body['order']['tax'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع تخفیف
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-2">ریال</sup>
                                                            {{number_format($body['order']['discount'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>مجموع کل
                                                        </div>
                                                        <div class="float-right">
                                                            <sup class="font-small-2">ریال</sup>
                                                            {{number_format($body['order']['total'])}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>شماره پیگیری
                                                        </div>
                                                        <div class="float-right">
                                                            {{$body['order']['transaction']['trace_num']}}
                                                        </div>
                                                    </div>
                                                    <div class="d-flex justify-content-between mt-2 font-small-2">
                                                        <div class="float-left">
                                                            <i class="feather icon-chevron-left text-warning mr-50"></i>تاریخ تراکنش
                                                        </div>
                                                        <div class="float-right">
                                                            {{\verta($body['order']['transaction']['created_at']->toDateString())->format('Y/m/d')}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chart-info d-flex justify-content-between mb-2">
                                        <div class="series-info d-flex align-items-center pt-1">
                                            <span class="text-bold-600 text-dark font-medium-1 mx-50">مبلغ قابل پرداخت</span>
                                        </div>
                                        <div class="series-result pt-1">
                                            <p class="text-right text-success font-medium-1">
                                                <sup class="font-medium-1">ریال</sup>
                                                {{number_format($body['order']['payable'])}}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section id="data-list-view" class="data-list-view-header">
                    <div class="card">
                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">اقلام سفارش</div>
                            </div>
                        </div>
                        <div class="card-content">
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>کد محصول</th>
                                        <th>نام محصول</th>
                                        <th>تعداد</th>
                                        <th>تخفیف (%)</th>
                                        <th>قیمت پایه (ریال)</th>
                                        <th>قیمت پس از تخفیف (ریال)</th>
                                        <th>مجموع (ریال)</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['items']))
                                        @foreach($body['items'] as $item)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$item['sku']}}</td>
                                                <td>{{$item['name']}}</td>
                                                <td>{{$item['qty']}}</td>
                                                <td>{{number_format($item['discount'])}}</td>
                                                <td>{{number_format($item['base_price'])}}</td>
                                                <td>{{number_format($item['price_after_discount'])}}</td>
                                                <td>{{number_format($item['qty'] * $item['price_after_discount'])}}</td>
                                                <td class="product-action">
                                                    <a href="{{--{{route('withdrawal_show', ['withdrawal_id' => $withdrawal['id']])}}--}}" data-toggle="tooltip" data-placement="top" title="پروفایل محصول"><span class="action-edit font-medium-5"><i class="feather icon-info"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script>
        function printDiv()
        {

            var divToPrint=document.getElementById('invoice');

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write("<!DOCTYPE html>\n" +
                "<html lang=\"fa\" dir=\"rtl\">\n" +
                "\t<head>\n" +
                "\t\t<meta charset=\"utf-8\" />\n" +
                "\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n" +
                "\n" +
                "\t\t<title>فاکتور فروش</title>\n" +
                "\n" +
                "\t\t<!-- Favicon -->\n" +
                "\t\t<link rel=\"icon\" href=\"./images/favicon.png\" type=\"image/x-icon\" />\n" +
                "\n" +
                "\t\t<!-- Invoice styling -->\n" +
                "\t\t<style>\n" +
                "\t\t\tbody {\n" +
                "\t\t\t\tfont-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
                "\t\t\t\ttext-align: center;\n" +
                "\t\t\t\tcolor: #777;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\tbody h1 {\n" +
                "\t\t\t\tfont-weight: 300;\n" +
                "\t\t\t\tmargin-bottom: 20px;\n" +
                "\t\t\t\tpadding-bottom: 0px;\n" +
                "\t\t\t\tcolor: #000;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\tbody h3 {\n" +
                "\t\t\t\tfont-weight: 300;\n" +
                "\t\t\t\tmargin-top: 10px;\n" +
                "\t\t\t\tmargin-bottom: 20px;\n" +
                "\t\t\t\tfont-style: italic;\n" +
                "\t\t\t\tcolor: #555;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\tbody a {\n" +
                "\t\t\t\tcolor: #06f;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box {\n" +
                "\t\t\t\tmax-width: 800px;\n" +
                "\t\t\t\tmargin: auto;\n" +
                "\t\t\t\tpadding: 30px;\n" +
                "\t\t\t\tborder: 1px solid #eee;\n" +
                "\t\t\t\tbox-shadow: 0 0 10px rgba(0, 0, 0, 0.15);\n" +
                "\t\t\t\tfont-size: 16px;\n" +
                "\t\t\t\tline-height: 24px;\n" +
                "\t\t\t\tfont-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;\n" +
                "\t\t\t\tcolor: #555;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table {\n" +
                "\t\t\t\twidth: 100%;\n" +
                "\t\t\t\tline-height: inherit;\n" +
                "\t\t\t\ttext-align: right;\n" +
                "\t\t\t\tborder-collapse: collapse;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table td {\n" +
                "\t\t\t\tpadding: 5px;\n" +
                "\t\t\t\tvertical-align: top;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr td:nth-child(2) {\n" +
                "\t\t\t\ttext-align: right;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.top table td {\n" +
                "\t\t\t\tpadding-bottom: 20px;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.top table td.title {\n" +
                "\t\t\t\tfont-size: 45px;\n" +
                "\t\t\t\tline-height: 45px;\n" +
                "\t\t\t\tcolor: #333;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.information table td {\n" +
                "\t\t\t\tpadding-bottom: 40px;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.heading td {\n" +
                "\t\t\t\tbackground: #eee;\n" +
                "\t\t\t\tborder-bottom: 1px solid #ddd;\n" +
                "\t\t\t\tfont-weight: bold;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.details td {\n" +
                "\t\t\t\tpadding-bottom: 20px;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.item td {\n" +
                "\t\t\t\tborder-bottom: 1px solid #eee;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.item.last td {\n" +
                "\t\t\t\tborder-bottom: none;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t.invoice-box table tr.total td:nth-child(2) {\n" +
                "\t\t\t\tborder-top: 2px solid #eee;\n" +
                "\t\t\t\tfont-weight: bold;\n" +
                "\t\t\t}\n" +
                "\n" +
                "\t\t\t@media only screen and (max-width: 600px) {\n" +
                "\t\t\t\t.invoice-box table tr.top table td {\n" +
                "\t\t\t\t\twidth: 100%;\n" +
                "\t\t\t\t\tdisplay: block;\n" +
                "\t\t\t\t\ttext-align: center;\n" +
                "\t\t\t\t}\n" +
                "\n" +
                "\t\t\t\t.invoice-box table tr.information table td {\n" +
                "\t\t\t\t\twidth: 100%;\n" +
                "\t\t\t\t\tdisplay: block;\n" +
                "\t\t\t\t\ttext-align: center;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +
                "\t\t</style>\n" +
                "\t</head>\n" +
                "\n" +
                "\t<body onload=\"window.print()\">\n" +
                "\n" +
                "\t\t<div class=\"invoice-box\">\n" +
                "\t\t\t<table>\n" +
                "\t\t\t\t<tr class=\"top\">\n" +
                "\t\t\t\t\t<td colspan=\"2\">\n" +
                "\t\t\t\t\t\t<table>\n" +
                "\t\t\t\t\t\t\t<tr>\n" +
                "\n" +
                "\t\t\t\t\t\t\t\t<td>\n" +
                "\t\t\t\t\t\t\t\t\tکد سفارش : {{$body['order']['code']}}<br />\n" +
                "\t\t\t\t\t\t\t\t\tتاریخ سفارش : {{verta($body['order']['created_at']->toDateString())->format('Y/m/d')}} <br />\n" +
                "\t\t\t\t\t\t\t\t</td>\n" +
                "\t\t\t\t\t\t\t\t<td class=\"title\">\n" +
                "\t\t\t\t\t\t\t\t\t<img src=\"{{asset("/images/logo/logo-type.png")}}\" alt=\"Company logo\" style=\"width: 100px;\" />\n" +
                "\t\t\t\t\t\t\t\t</td>\n" +
                "\t\t\t\t\t\t\t</tr>\n" +
                "\t\t\t\t\t\t</table>\n" +
                "\t\t\t\t\t</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"information\">\n" +
                "\t\t\t\t\t<td colspan=\"2\">\n" +
                "\t\t\t\t\t\t<table>\n" +
                "\t\t\t\t\t\t\t<tr>\n" +
                "\t\t\t\t\t\t\t\t<td>\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['order']['province']}} - {{$body['order']['city']}}</br>\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['order']['address']}}</br>\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['order']['postal_code']}}</br>\n" +
                "\t\t\t\t\t\t\t\t</td>\n" +
                "\n" +
                "\t\t\t\t\t\t\t\t<td>\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['user']['name']}}<br />\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['user']['national_id']}}<br />\n" +
                "\t\t\t\t\t\t\t\t\t{{$body['user']['mobile']}}\n" +
                "\t\t\t\t\t\t\t\t</td>\n" +
                "\t\t\t\t\t\t\t</tr>\n" +
                "\t\t\t\t\t\t</table>\n" +
                "\t\t\t\t\t</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"heading\">\n" +
                "\t\t\t\t\t<td>اقلام</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>تعداد</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"details\">\n" +
                "\t\t\t\t\t<td>مجموع اقلام</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>{{$body['order']['qty']}}</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"heading\">\n" +
                "\t\t\t\t\t<td>شرح هزینه</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>مبلغ (ریال)</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"item\">\n" +
                "\t\t\t\t\t<td>مجموع خرید</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>{{number_format($body['order']['subtotal'])}}</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"item\">\n" +
                "\t\t\t\t\t<td>هزینه حمل و نقل</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>{{number_format($body['order']['shipping'])}}</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"item last\">\n" +
                "\t\t\t\t\t<td>مجموع مالیات بر ارزش افزوده</td>\n" +
                "\n" +
                "\t\t\t\t\t<td>{{number_format($body['order']['tax'])}}</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\n" +
                "\t\t\t\t<tr class=\"total\">\n" +
                "\t\t\t\t\t<td></td>\n" +
                "\n" +
                "\t\t\t\t\t<td>مجموع کل: {{number_format($body['order']['total'])}}</td>\n" +
                "\t\t\t\t</tr>\n" +
                "\t\t\t</table>\n" +
                "\t\t</div>\n" +
                "\t</body>\n" +
                "</html>");

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);

        }
    </script>
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/pages/app-chat.js') }}"></script>
@endsection
