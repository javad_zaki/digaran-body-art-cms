@extends('layout.guest')

@section('title')
    <title></title>
@endsection

@section('meta')

    <meta name="description" content=""/>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/authentication.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <section class="row">
                    <div class="col-12 col-md-4 offset-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12 col-md-12 pt-3 my-auto">
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="card bg-transparent">
                                                <div class="card-header mx-auto pb-1">
                                                    <div class="avatar avatar-lg">
                                                        <img class="img-fluid" src="{{asset('/images/default/default.png')}}" alt="img placeholder">
                                                    </div>
                                                </div>
                                                <div class="card-content">
                                                    <h5 class="text-center">ورود به پنل مدیریتی</h5>
                                                    {!! $errors->first('message', '<div class="alert alert-primary alert-dismissible fade show mb-2" role="alert"><p class="mb-0 font-small-3">:message</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>') !!}
                                                    <div class="form-label-group mt-2">
                                                        <input type="text" id="mobile" name="mobile" class="form-control" placeholder="موبایل" value="{{ old('mobile') }}">
                                                        <label for="">موبایل</label>
                                                        {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                                    </div>
                                                    <div class="form-label-group">
                                                        <input type="password" id="password" name="password" class="form-control" placeholder="رمز عبور">
                                                        <label for="">رمز عبور</label>
                                                        {!! $errors->first('password', '<span class="text-warning font-small-2">:message</span>') !!}
                                                    </div>
                                                    <div class="form-group row">
                                                        @if (Route::has('password.request'))
                                                            <div class="col-12">
                                                                <span class="font-small-3">آیا رمز عبور خود را <a href="{{route('password.request')}}">فراموش کرده اید</a>؟</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-block">ورود</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection
