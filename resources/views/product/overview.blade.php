@extends('layout.app')

@section('title')
    <title>پروفایل محصول</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-ecommerce.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">پروفایل محصول</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/products'}}">محصول ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">{{$body['product']['sku']}}</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section class="app-ecommerce-details">
                    <div class="card">
                        <div class="card-body">
                            <div class="row mb-5 mt-2">
                                <div class="col-12 col-md-5 d-flex align-items-center justify-content-center mb-2 mb-md-0">
                                    <div class="d-flex align-items-center justify-content-center">
                                        <img src="{!! (!is_null($body['product']['primary'])) ? html_entity_decode(e($body['product']['primary']['thumbnail_file_name'])) : asset('/images/default/product.png') !!}" class="img-fluid" alt="product image">
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <p class="text-muted">{{$body['product']['category']['name']}}</p>
                                    <h5>{{$body['product']['name']}}</h5>
                                    <div class="ecommerce-details-price d-flex flex-wrap mt-2">
                                        <p class="text-primary font-medium-3 mr-1 mb-0"><sup class="font-small-2">ریال</sup>{{number_format($body['product']['base_price'])}}<sup class="font-small-3 text-dark"> قیمت پایه </sup></p>
                                        <span class="pl-1 font-medium-3 border-left"></span>
                                        <p class="text-primary font-medium-3 mr-1 mb-0"><sup class="font-small-2">%</sup>{{$body['product']['discount']}}<sup class="font-small-3 text-dark"> تخفیف </sup></p>
                                        <span class="pl-1 font-medium-3 border-left"></span>
                                        <p class="text-primary font-medium-3 mr-1 mb-0"><sup class="font-small-2">ریال</sup>{{number_format($body['product']['price_after_discount'])}}<sup class="font-small-3 text-dark"> قیمت پس از تخفیف </sup></p>
                                    </div>
                                    <hr>
                                    <p>{!! nl2br(e($body['product']['short_description'])) !!}</p>

                                    <div class="accordion" id="accordionExample" data-toggle-hover="true">
                                        <div class="collapse-margin">
                                            <div class="card-header collapsed" id="headingOne" data-toggle="collapse" role="button" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                <span class="lead collapse-title">
                                                    توضیحات فنی
                                                </span>
                                            </div>

                                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                                <div class="card-body">
                                                    {!! nl2br(e($body['product']['technical_description'])) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <ul class="list-group">
                                        @foreach(json_decode($body['product']['properties'], true) as $item)
                                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                                <span>{{$item['key']}}</span>
                                                <span class="badge badge-primary badge-pill">{{$item['value']}}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <hr>
                                    <p>وضعیت :
                                        @switch($body['product']['status'])
                                            @case('enable')
                                                <span class="text-success">فعال</span>
                                            @break
                                            @case('disable')
                                                <span class="text-danger">غیر فعال</span>
                                            @break
                                        @endswitch
                                    </p>
                                    <div class="d-flex flex-column flex-sm-row">
                                        <a href="{{route('products.color', ['id' => $body['product']['id']])}}" class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">افزودن رنگ</a>
                                        <a href="{{route('products.update', ['id' => $body['product']['id']])}}" class="btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">ویرایش</a>
                                        <a href="{{route('products.enable', ['id' => $body['product']['id']])}}" class="btn btn-success mr-0 mr-sm-1 mb-1 mb-sm-0 waves-effect waves-light">فعال</a>
                                        <a href="{{route('products.disable', ['id' => $body['product']['id']])}}" class="btn btn-outline-danger waves-effect waves-light">غیر فعال</a>
                                    </div>
                                    <hr>
                                    @foreach(explode(',', $body['product']['tags']) as $item)
                                        <div class="chip mr-1">
                                            <div class="chip-body">
                                                <div class="avatar">
                                                    <i class="feather icon-tag"></i>
                                                </div>
                                                <span class="chip-text">{{$item}}</span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="card" style="">
                        <div class="card-content">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    @if(!empty($body['product']['catalog']))
                                        @foreach($body['product']['catalog'] as $key => $item)
                                            <li data-target="#carousel-example-generic" data-slide-to="{{$key}}" class="{{($key == 0) ? 'active' : ''}}"></li>
                                        @endforeach
                                    @else
                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    @endif
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    @if(!empty($body['product']['catalog']))
                                        @foreach($body['product']['catalog'] as $item)
                                            <div class="carousel-item {{($key == 0) ? 'active' : ''}}">
                                                <img src="{!! html_entity_decode(e($item['thumbnail_file_name'])) !!}" class="d-block w-100" alt="">
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="carousel-item active">
                                            <img src="{{asset('/images/default/product-01-promo-01.png')}}" class="d-block w-100" alt="">
                                        </div>
                                    @endif
                                </div>
                                <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="fa fa-angle-left icon-prev" aria-hidden="true"></span>
                                    <span class="sr-only">قبلی</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="fa fa-angle-right icon-next" aria-hidden="true"></span>
                                    <span class="sr-only">بعدی</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/pages/app-chat.js') }}"></script>
@endsection
