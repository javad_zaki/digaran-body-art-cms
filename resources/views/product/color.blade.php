@extends('layout.app')

@section('title')
    <title>افزودن رنگ محصول</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/file-uploaders/filepond-plugin-image-preview.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/select/select2.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">افزودن رنگ محصول</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/products'}}">محصول ها</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{route('products.overview',['id' => $body['id']])}}">محصول</a>
                                    </li>
                                    <li class="breadcrumb-item active">افزودن رنگ محصول</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                    <section>
                        <div class="row">
                            <div class="col-12 col-sm-12">
                                <form class="form" enctype="multipart/form-data" method="POST" action="{{route('products.store.color')}}">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$body['id']}}">
                                    <div class="col-12 col-md-8 offset-md-2">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="divider divider-left mb-2">
                                                    <div class="divider-text font-medium-2">فرم افزودن رنگ محصول</div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-12">
                                                        <div class="form-body">

                                                            <div class="row">
                                                                <div class="col-12 col-md-6">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="color" placeholder=" رنگ محصول" maxlength="300"/>
                                                                        </div>
                                                                        {!! $errors->first('color', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 col-md-6">
                                                                    <fieldset>
                                                                        <div class="input-group">
                                                                            <input type="text" class="form-control" name="hex" placeholder="کد رنگ محصول (HEX)" maxlength="300"/>
                                                                        </div>
                                                                        {!! $errors->first('hex', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                    </fieldset>
                                                                </div>
                                                                <div class="col-12 mt-2">
                                                                    <input type="file" id="color" name="color" class="filepond"
                                                                           accept="image/png, image/jpg, image/jpeg"
                                                                           data-allow-image-preview="true"
                                                                           data-instant-upload="false">
                                                                    {!! $errors->first('color', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                            </div>

                                                            @if(!$body['colors']->isEmpty())
                                                                <div class="row">
                                                                    @foreach($body['colors'] as $key => $item)
                                                                        <div class="col-12 col-lg-3">
                                                                            <div class="card" style="background-color: #efefef">
                                                                                <div class="card-content">
                                                                                    <img class="card-img-top img-fluid" src="{!! html_entity_decode(e($item['thumbnail_file_name'])) !!}" alt="">
                                                                                    <div class="card-body">
                                                                                        <div class="card-btns d-flex justify-content-between mt-2">
                                                                                            <a href="{{route('products.delete.multimedia', ['id' => $item['id']])}}" class="btn btn-sm btn-block gradient-light-danger text-white waves-effect waves-light">حذف</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                                <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </section>


            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('vendor-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-encode.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-type.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-file-validate-size.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-exif-orientation.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/file-uploaders/filepond-plugin-image-preview.min.js') }}"></script>
@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/select/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
@endsection

@section('custom-js')
    <script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>
    <script src="{{ asset('js/scripts/forms/number-input.js') }}"></script>
    <script>

        FilePond.registerPlugin(
            FilePondPluginFileEncode,
            FilePondPluginFileValidateType,
            FilePondPluginFileValidateSize,
            FilePondPluginImagePreview,
            FilePondPluginImageExifOrientation,
        );

        const color = FilePond.create(
            document.getElementById('color'),
        );

        color.setOptions({
            allowFileSizeValidation:true,
            maxFiles: 1,
            maxParallelUploads: 1,
            allowMultiple: false,
            labelIdle:'برای آپلود کردن تصویر شاخص محصول اینجا کلیک کنید',
        });

    </script>
@endsection
