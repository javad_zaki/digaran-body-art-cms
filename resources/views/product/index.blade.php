@extends('layout.app')

@section('title')
    <title>محصول ها</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/datatables.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/data-list-view.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">محصول ها</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">داشبورد</a>
                                    </li>
                                    <li class="breadcrumb-item active">محصول ها</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif

                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-box text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_enable_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye-off text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_disable_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها غیر فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>


                <section id="data-list-view" class="data-list-view-header">
                    <div class="card">

                        <div class="card-body">
                            <div class="divider divider-left mb-2">
                                <div class="divider-text font-medium-3 text-bold-600">محصول ها</div>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="actions action-btns">
                                <div class="dt-buttons btn-group">
                                    <a href="{{route('products.create')}}">
                                        <button class="btn btn-primary">
                                            <span><i class="feather icon-plus"></i>افزودن محصول</span>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="card-content">

                            <!-- DataTable starts -->
                            <div class="table-responsive">
                                <table class="table data-list-view">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>شناسه</th>
                                        <th>نام</th>
                                        <th>موجودی</th>
                                        <th>تخفیف (%)</th>
                                        <th>قیمت پایه (ریال)</th>
                                        <th>قیمت پس از تخفیف (ریال)</th>
                                        <th>دسته</th>
                                        <th>وضعیت</th>
                                        <th>نوع</th>
                                        <th>عملیات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($body['products']))
                                        @foreach($body['products'] as $product)
                                            <tr>
                                                <td>#</td>
                                                <td>{{$product['sku']}}</td>
                                                <td>{{$product['name']}}</td>
                                                <td>{{$product['qty']}}</td>
                                                <td>{{number_format($product['discount'])}}</td>
                                                <td>{{number_format($product['base_price'])}}</td>
                                                <td>{{number_format($product['price_after_discount'])}}</td>
                                                <td>
                                                    @switch($product['domain'])

                                                        @case('tattoo')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">تاتو</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('piercing')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">پیرسینگ</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td>
                                                    @switch($product['status'])

                                                        @case('enable')
                                                        <div class="chip chip-success">
                                                            <div class="chip-body">
                                                                <div class="chip-text">فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('disable')
                                                        <div class="chip chip-danger">
                                                            <div class="chip-body">
                                                                <div class="chip-text">غیر فعال</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td>
                                                    @switch($product['type'])

                                                        @case('featured')
                                                        <div class="chip chip-warning">
                                                            <div class="chip-body">
                                                                <div class="chip-text">ویژه</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                        @case('general')
                                                        <div class="chip chip-info">
                                                            <div class="chip-body">
                                                                <div class="chip-text">عمومی</div>
                                                            </div>
                                                        </div>
                                                        @break

                                                    @endswitch
                                                </td>
                                                <td class="product-action">
                                                    <a href="{{route('products.overview', ['id' => $product['id']])}}" data-toggle="tooltip" data-placement="top" title="پروفایل محصول"><span class="action-edit font-medium-5"><i class="feather icon-info"></i></span></a>
                                                    <a href="{{route('products.enable', ['id' => $product['id']])}}" data-toggle="tooltip" data-placement="top" title="فعال"><span class="action-edit font-medium-5"><i class="feather icon-eye"></i></span></a>
                                                    <a href="{{route('products.disable', ['id' => $product['id']])}}" data-toggle="tooltip" data-placement="top" title="غیر فعال"><span class="action-edit font-medium-5"><i class="feather icon-eye-off"></i></span></a>
                                                    <a href="{{route('products.featured', ['id' => $product['id']])}}" data-toggle="tooltip" data-placement="top" title="ویژه"><span class="action-edit font-medium-5"><i class="feather icon-heart"></i></span></a>
                                                    <a href="{{route('products.general', ['id' => $product['id']])}}" data-toggle="tooltip" data-placement="top" title="عمومی"><span class="action-edit font-medium-5"><i class="feather icon-globe"></i></span></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- DataTable ends -->
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section("page-js")
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/dataTables.select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
@endsection


@section("custom-js")
    <script type="text/javascript" src="{{ asset('/js/scripts/ui/data-list-view.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/datatables/datatable.js') }}"></script>
@endsection
