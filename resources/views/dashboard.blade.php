@extends('layout.app')

@section('title')
    <title>داشبورد</title>
@endsection


@section('meta')
    <meta name="description" content=""/>
@endsection

@section("page-css")
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/pages/dashboard-analytics.css') }}" type="text/css" />
@endsection


@section('body')

    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-body">
                {!! $errors->first('message', '<div class="alert alert-primary alert-dismissible fade show" role="alert"><p class="mb-0">:message</p><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="feather icon-x-circle"></i></span></button></div>') !!}

                <section>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-shopping-bag text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['pending_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های جدید</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-shopping-bag text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="row">
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-package text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['queued_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های در حال بسته بندی</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-truck text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['sent_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">سفارش های ارسال شده</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('financial_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-refresh-cw text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['returned_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis text-white">سفارش های مرجوعی</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-3">
                            <a href="{{--{{route('financial_index')}}--}}">
                                <div class="card text-center bg-gradient-primary text-white">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-credit-card text-info font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['unpaid_orders_count']}}</h2>
                                            <p class="mb-0 line-ellipsis text-white">سفارش های پرداخت نشده</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری سفارش ها</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-orders-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-success">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-shopping-bag text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{number_format($body['total_orders_amount'])}}</h2>
                                            <p class="mb-0 line-ellipsis">مبلغ کل سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-success">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-percent text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{number_format($body['total_orders_discount_amount'])}}</h2>
                                            <p class="mb-0 line-ellipsis">مبلغ کل تخفیف سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-success">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-percent text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{number_format($body['total_orders_tax_amount'])}}</h2>
                                            <p class="mb-0 line-ellipsis">مبلغ کل مالیات سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-success">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-truck text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{number_format($body['total_orders_shipping_amount'])}}</h2>
                                            <p class="mb-0 line-ellipsis">مبلغ کل باربری سفارش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-success">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-refresh-cw text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{number_format($body['total_returned_orders_amount'])}}</h2>
                                            <p class="mb-0 line-ellipsis">مبلغ کل سفارش های مرجوعی</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری (سفارش های موفق و مرجوعی)</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-paid-and-returned-orders-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-info">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-users text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_customers_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل مشتری ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-info">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-user text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['guest_customers_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">مشتری های مهمان</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-info">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-user-check text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['member_customers_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">مشتری های عضو</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری مشتری های عضو</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-member-customers-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری مشتری های مهمان</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-guest-customers-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-box text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_enable_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye-off text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_disable_products_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل محصول ها غیر فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری محصول ها</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-products-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-play-circle text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_courses_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل آموزش ها</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_enable_courses_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل آموزش های فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-danger">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-eye-off text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_disable_courses_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل آموزش های غیر فعال</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری آموزش ها</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-courses-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


                <section>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-dark">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-users text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_artists_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل هنرمندان</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-dark">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-user-check text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_artists_author_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل هنرمندان مدرس</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="{{--{{route('contract_index')}}--}}">
                                <div class="card text-center bg-gradient-dark">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <div class="avatar bg-rgba-white p-50 m-0 mb-1">
                                                <div class="avatar-content">
                                                    <i class="feather icon-user text-white font-medium-5"></i>
                                                </div>
                                            </div>
                                            <h2 class="text-bold-700 text-white">{{$body['total_artists_artist_count']}}</h2>
                                            <p class="mb-0 line-ellipsis">کل هنرمندان غیر مدرس</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </section>

                <section>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="divider divider-left mb-2">
                                        <div class="divider-text font-medium-3 text-bold-600">نمودار خطی سال جاری هنرمندان</div>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body pl-0">
                                        <div class="height-300">
                                            <canvas id="annual-artists-line-chart"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/charts/chart.min.js') }}"></script>
@endsection

@section('custom-js')

    <script>

        $(window).on("load", function () {

            Chart.defaults.global.defaultFontFamily = "IRANSans_num";
            var $primary = '#7367F0';
            var $success = '#28C76F';
            var $danger = '#EA5455';
            var $warning = '#FF9F43';
            var $info = '#2D91FF';
            var $label_color = '#1E1E1E';
            var grid_line_color = '#dae1e7';
            var scatter_grid_color = '#f3f3f3';
            var $scatter_point_light = '#D1D4DB';
            var $white = '#fff';
            var $black = '#000';
            var themeColors = [$primary, $success, $danger, $warning, $label_color];

            // Line Chart Options
            var lineChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    position: 'top',
                },
                hover: {
                    mode: 'label'
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: grid_line_color,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }],
                    yAxes: [{
                        display: true,
                        gridLines: {
                            color: grid_line_color,
                        },
                        scaleLabel: {
                            display: true,
                        }
                    }]
                },
                title: {
                    display: true,
                    text: 'نمودار خطی سال جاری'
                }
            };


            var annual_orders_line_chart = $("#annual-orders-line-chart");
            var annual_orders_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "سفارش ها",
                    data: {{$body['annual_orders_chart_data']}},
                    borderColor: $primary,
                    fill: true
                }]
            };
            var annual_orders_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_orders_line_chart_data
            };
            var annual_orders_line_chart_obj = new Chart(annual_orders_line_chart, annual_orders_line_chart_config);


            var annual_paid_and_returned_orders_line_chart = $("#annual-paid-and-returned-orders-line-chart");
            var annual_paid_and_returned_orders_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [
                    {
                        label: "سفارش های موفق",
                        data: {{$body['annual_paid_orders_chart_data']}},
                        borderColor: $primary,
                        fill: true
                    },
                    {
                        label: "سفارش های مرجوعی",
                        data: {{$body['annual_returned_orders_chart_data']}},
                        borderColor: $danger,
                        fill: true
                    },
                ]
            };
            var annual_paid_and_returned_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_paid_and_returned_orders_line_chart_data
            };
            var annual_paid_and_returned_orders_line_chart_obj = new Chart(annual_paid_and_returned_orders_line_chart, annual_paid_and_returned_line_chart_config);


            var annual_member_customers_line_chart = $("#annual-member-customers-line-chart");
            var annual_member_customers_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "مشتری های عضو",
                    data: {{$body['annual_member_customers_chart_data']}},
                    borderColor: $info,
                    fill: true
                }]
            };
            var annual_member_customers_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_member_customers_line_chart_data
            };
            var annual_member_customers_line_chart_obj = new Chart(annual_member_customers_line_chart, annual_member_customers_line_chart_config);

            var annual_guest_customers_line_chart = $("#annual-guest-customers-line-chart");
            var annual_guest_customers_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "مشتری های عضو",
                    data: {{$body['annual_guest_customers_chart_data']}},
                    borderColor: $info,
                    fill: true
                }]
            };
            var annual_guest_customers_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_guest_customers_line_chart_data
            };
            var annual_guest_customers_line_chart_obj = new Chart(annual_guest_customers_line_chart, annual_guest_customers_line_chart_config);


            var annual_products_line_chart = $("#annual-products-line-chart");
            var annual_products_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "محصول ها",
                    data: {{$body['annual_products_chart_data']}},
                    borderColor: $danger,
                    fill: true
                }]
            };
            var annual_products_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_products_line_chart_data
            };
            var annual_products_line_chart_obj = new Chart(annual_products_line_chart, annual_products_line_chart_config);


            var annual_courses_line_chart = $("#annual-courses-line-chart");
            var annual_courses_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [{
                    label: "آموزش ها",
                    data: {{$body['annual_courses_chart_data']}},
                    borderColor: $danger,
                    fill: true
                }]
            };
            var annual_courses_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_courses_line_chart_data
            };
            var annual_courses_line_chart_obj = new Chart(annual_courses_line_chart, annual_courses_line_chart_config);


            var annual_artists_line_chart = $("#annual-artists-line-chart");
            var annual_artists_line_chart_data = {
                labels: ['فروردین','اردیبهشت','خرداد','تیر','مرداد','شهریور','مهر','آبان','آذر','دی','بهمن','اسفند'],
                datasets: [
                    {
                        label: "هنرمندان مدرس",
                        data: {{$body['annual_artists_author_chart_data']}},
                        borderColor: $primary,
                        fill: true
                    },
                    {
                        label: "هنرمندان غیر مدرس",
                        data: {{$body['annual_artists_artist_chart_data']}},
                        borderColor: $danger,
                        fill: true
                    },
                ]
            };
            var annual_artists_line_chart_config = {
                type: 'line',
                options: lineChartOptions,
                data: annual_artists_line_chart_data
            };
            var annual_artists_line_chart_obj = new Chart(annual_artists_line_chart, annual_artists_line_chart_config);

        });

    </script>

@endsection


