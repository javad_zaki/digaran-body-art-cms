<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl">
<head>
    <!--=================================================================================================================-->
    @section('title')@show

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">


    @section('meta')@show

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset("images/ico/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset("images/ico/apple-icon-60x60.png")}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset("images/ico/apple-icon-72x72.png")}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset("images/ico/apple-icon-76x76.png")}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset("images/ico/apple-icon-114x114.png")}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset("images/ico/apple-icon-120x120.png")}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset("images/ico/apple-icon-144x144.png")}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset("images/ico/apple-icon-152x152.png")}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset("images/ico/apple-icon-180x180.png")}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset("images/ico/android-icon-192x192.png")}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset("images/ico/favicon-32x32.png")}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset("images/ico/favicon-96x96.png")}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("images/ico/favicon-16x16.png")}}">
    <link rel="manifest" href="{{asset("images/ico/manifest.json")}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!--=================================================================================================================-->

    <!--=================================================================================================================-->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" href="{{ asset('/vendors/css/vendors-rtl.min.css') }}" type="text/css" />
    @section('vendor-css')@show
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="{{ asset('/css-rtl/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/bootstrap-extended.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/colors.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/css-rtl/components.css') }}" type="text/css" />
    @section('theme-css')@show
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/menu/menu-types/horizontal-menu.css') }}" type="text/css" />
    @section('page-css')@show
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" href="{{ asset('/css-rtl/custom-rtl.css') }}" type="text/css" />
    @section('custom-css')@show
    <!-- END: Custom CSS-->

    <!--=================================================================================================================-->

</head>


    <body class="footer-static bg-full-screen-image" data-open="hover" data-menu="horizontal-menu" data-col="1-column">
        @section('body')@show

        <!-- BEGIN: Footer-->
        <footer class="footer footer-static">
            <p class="clearfix text-center white lighten-2 mb-0">
                <span class="font-small-1 d-block d-md-inline-block">تمامی حقوق مادی و معنوی این فروشگاه مربوط به برند دیگران می باشد.</span>
            </p>
        </footer>
        <!-- END: Footer-->

    </body>


    <!--=================================================================================================================-->

    <!-- BEGIN: Vendor JS-->
    <script type="text/javascript" src="{{ asset('/vendors/js/vendors.min.js') }}"></script>
    @section('vendor-js')@show
    <!-- END: Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script type="text/javascript" src="{{ asset('/vendors/js/ui/jquery.sticky.js') }}"></script>
    @section('page-js')@show
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script type="text/javascript" src="{{ asset('/js/core/app-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/components.js') }}"></script>
    @section('theme-js')@show
    <!-- END: Theme JS-->


    <!-- BEGIN: Custom JS-->
    @section('custom-js')@show
    <!-- END: Custom JS-->

    <!--=================================================================================================================-->
</html>
