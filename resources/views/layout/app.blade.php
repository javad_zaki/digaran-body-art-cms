<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl">
<head>

    <!--=================================================================================================================-->
    @section('title')@show

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    @section('meta')@show

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset("images/ico/apple-icon-57x57.png")}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset("images/ico/apple-icon-60x60.png")}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset("images/ico/apple-icon-72x72.png")}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset("images/ico/apple-icon-76x76.png")}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset("images/ico/apple-icon-114x114.png")}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset("images/ico/apple-icon-120x120.png")}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset("images/ico/apple-icon-144x144.png")}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset("images/ico/apple-icon-152x152.png")}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset("images/ico/apple-icon-180x180.png")}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset("images/ico/android-icon-192x192.png")}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset("images/ico/favicon-32x32.png")}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset("images/ico/favicon-96x96.png")}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset("images/ico/favicon-16x16.png")}}">
    <link rel="manifest" href="{{asset("images/ico/manifest.json")}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!--=================================================================================================================-->

    <!--=================================================================================================================-->

        <!-- BEGIN: Vendor CSS-->
        <link rel="stylesheet" href="{{ asset('/vendors/css/vendors-rtl.min.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/vendors/css/ui/prism.min.css') }}" type="text/css" />
        @section('vendor-css')@show
        <!-- END: Vendor CSS-->

        <!-- BEGIN: Theme CSS-->
        <link rel="stylesheet" href="{{ asset('/css-rtl/bootstrap.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css-rtl/bootstrap-extended.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css-rtl/colors.css') }}" type="text/css" />
        <link rel="stylesheet" href="{{ asset('/css-rtl/components.css') }}" type="text/css" />
        @section('theme-css')@show
        <!-- END: Theme CSS-->

        <!-- BEGIN: Page CSS-->
        <link rel="stylesheet" href="{{ asset('/css-rtl/core/menu/menu-types/horizontal-menu.css') }}" type="text/css" />
        @section('page-css')@show
        <!-- END: Page CSS-->

        <!-- BEGIN: Custom CSS-->
        <link rel="stylesheet" href="{{ asset('/css-rtl/custom-rtl.css') }}" type="text/css" />
        @section('custom-css')@show
        <!-- END: Custom CSS-->


    <!--=================================================================================================================-->


</head>

<body class="horizontal-layout horizontal-menu 2-columns  navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <div class="content-overlay"></div>
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu navbar-fixed navbar-brand-center">
        <div class="navbar-header d-xl-block d-none">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item">
                    <a class="navbar-brand" href="{{url("/")}}">
                        <div class="brand-logo"></div>
                    </a>
                </li>
            </ul>
        </div>
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <li class="nav-item mobile-menu d-xl-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a></li>
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{request()->user()->full_name}}</span>
                                    <span class="user-status">{{(request()->user()->status == 'enable') ? 'فعال' : 'غیرفعال'}}</span>
                                </div>
                                <span>
                                    <img class="round" src="{{asset('/images/default/default.png')}}" alt="avatar" height="40" width="40">
                                </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item"
                                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="feather icon-power"></i> خروج </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- END: Header-->


    <!-- BEGIN: Main Menu-->
    <div class="horizontal-menu-wrapper">
        <div class="header-navbar navbar-expand-sm navbar navbar-horizontal floating-nav navbar-light navbar-without-dd-arrow navbar-shadow menu-border" role="navigation" data-menu="menu-wrapper">
            <!-- Horizontal menu content-->
            <div class="navbar-container main-menu-content" data-menu="menu-container">
                <ul class="nav navbar-nav" id="main-menu-navigation" data-menu="menu-navigation">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{route('dashboard')}}"><i class="feather icon-grid"></i><span>داشبورد</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users')}}"><i class="feather icon-users"></i><span>مشتری ها</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('orders')}}"><i class="feather icon-shopping-bag"></i><span>سفارش ها</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('categories')}}"><i class="feather icon-list"></i><span>دسته بندی ها</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('products')}}"><i class="feather icon-box"></i><span>محصول ها</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('courses')}}"><i class="feather icon-play-circle"></i><span>آموزش ها</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('artists')}}"><i class="feather icon-users"></i><span>هنرمندان</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('financial')}}"><i class="feather icon-dollar-sign"></i><span>مالی</span></a>
                    </li>
                    <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="feather icon-settings"></i><span>مدیریت اپراتورها</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                                <li data-menu="">
                                    <a class="dropdown-item" href="{{route('operators')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>اپراتورها</a>
                                    @role('super-admin')
                                    <a class="dropdown-item" href="{{route('permissions')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>مجوزها</a>
                                    <a class="dropdown-item" href="{{route('roles')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>نقش ها</a>
                                    @endrole
                                </li>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown nav-item" data-menu="dropdown"><a class="dropdown-toggle nav-link" href="#" data-toggle="dropdown"><i class="feather icon-settings"></i><span>مدیریت محتوا</span></a>
                        <ul class="dropdown-menu">
                            <li class="dropdown dropdown-submenu" data-menu="dropdown-submenu">
                                <li data-menu="">
                                    <a class="dropdown-item" href="{{route('content.sliders')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>اسلایدر اصلی</a>
                                    <a class="dropdown-item" href="{{route('content.promos')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>پروموشن ها</a>
                                    <a class="dropdown-item" href="{{route('content.shipping')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>تنظیمات باربری</a>
                                    <a class="dropdown-item" href="{{route('content.tax')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>تنظیمات مالیات</a>
                                    <a class="dropdown-item" href="{{route('content.supports')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>تنظیمات پشتیبانی</a>
                                    <a class="dropdown-item" href="{{route('content.abouts')}}" data-toggle="dropdown"><i class="feather icon-chevron-left"></i>تنظیمات درباره ما</a>
                                </li>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    @section('body')@show


    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light navbar-shadow">
        <p class="clearfix blue-grey lighten-2 mb-0"><span class="float-md-left font-small-1 d-block d-md-inline-block mt-25">تمامی حقوق مادی و معنوی این فروشگاه متعلق به برند دیگران می باشد.</span>
            <span class="float-md-right font-small-1 d-none d-md-block">نسخه 1.0</span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>
    </footer>
    <!-- END: Footer-->


    <!--=================================================================================================================-->

    <!-- BEGIN: Vendor JS-->
    <script type="text/javascript" src="{{ asset('/vendors/js/vendors.min.js') }}"></script>
    @section('vendor-js')@show
    <!-- END: Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script type="text/javascript" src="{{ asset('/vendors/js/ui/jquery.sticky.js') }}"></script>
    @section('page-js')@show
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script type="text/javascript" src="{{ asset('/js/core/app-menu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/scripts/components.js') }}"></script>
    @section('theme-js')@show
    <!-- END: Theme JS-->


    <!-- BEGIN: Custom JS-->
    @section('custom-js')@show
    <!-- END: Custom JS-->

    <script type="text/javascript" src="{{ asset('/vendors/js/ui/moment-with-locales.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/vendors/js/ui/moment-timezone-with-data.js') }}"></script>
    <script type="text/javascript">
        let notificationtime = document.querySelectorAll('[notificationtime]');
        [].forEach.call(notificationtime, function(el) {
            el.innerHTML = moment(el.getAttribute('notificationtime '), "YYYY-MM-DD H:i:s").tz("Asia/Tehran").locale('fa').fromNow();
        });
    </script>
    <!--=================================================================================================================-->

</body>

</html>
