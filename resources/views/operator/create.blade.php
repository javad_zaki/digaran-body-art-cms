@extends('layout.app')

@section('title')
    <title>افزودن اپراتور</title>
@endsection

@section('meta')
    <meta name="description" content=""/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('vendor-css')
    <link rel="stylesheet" href="{{ asset('/vendors/css/forms/select/select2.min.css') }}" type="text/css" />
@endsection

@section('page-css')
    <link rel="stylesheet" href="{{ asset('/css-rtl/core/colors/palette-gradient.css') }}" type="text/css" />
@endsection


@section('body')

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2">
                    <div class="row breadcrumbs-top">
                        <div class="col-12">
                            <h2 class="content-header-title float-left mb-0">افزودن اپراتور</h2>
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="{{'/'}}">خانه</a>
                                    </li>
                                    <li class="breadcrumb-item">
                                        <a href="{{'/operators'}}">اپراتور ها</a>
                                    </li>
                                    <li class="breadcrumb-item active">افزودن اپراتور</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                @if(session()->has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        <span class="text-dark">{{session()->get('message')}}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="feather icon-x"></i>
                        </button>
                    </div>
                @endif
                <section>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <form class="form" method="POST" action="{{route('operators.store')}}">
                                @csrf
                                <div class="col-12 col-md-8 offset-md-2">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="divider divider-left mb-2">
                                                <div class="divider-text font-medium-2">فرم افزودن اپراتور</div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-12">
                                                    <div class="form-body">
                                                        <div class="row">
                                                            <div class="col-12 col-md-6">
                                                                <div class="form-group">
                                                                    <select class="select2 form-control" name="roles[]" multiple="multiple" autofocus>
                                                                        @if(!empty($body['roles']))
                                                                            @foreach($body['roles'] as $role)
                                                                                <option value="{{$role['name']}}">{{$role['name']}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    </select>
                                                                    {!! $errors->first('roles', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="full_name" placeholder="نام اپراتور" maxlength="300"/>
                                                                    </div>
                                                                    {!! $errors->first('full_name', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="mobile" placeholder="شماره موبایل" maxlength="11"/>
                                                                    </div>
                                                                    {!! $errors->first('mobile', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                            <div class="col-12 col-md-6">
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="email" placeholder="ایمیل" maxlength="150"/>
                                                                    </div>
                                                                    {!! $errors->first('email', '<span class="text-warning font-small-2">:message</span>') !!}
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light">ذخیره</button>
                                            <button type="reset" class="btn btn-outline-warning">پاک کردن فرم</button>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

@section('page-js')
    <script type="text/javascript" src="{{ asset('/vendors/js/forms/select/select2.full.min.js') }}"></script>
@endsection

@section('custom-js')
    <script src="{{ asset('js/scripts/forms/select/form-select2.js') }}"></script>
@endsection
