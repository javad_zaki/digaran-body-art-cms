<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'اطلاعات کاربری شما مجاز نمی باشد.',
    'password' => 'رمز عبور وارد شده صحیح نمی باشد.',
    'throttle' => 'درخواست های ورود پیاپی. لطفا پس از :seconds ثانیه، مجددا تلاش کنید.',

];
