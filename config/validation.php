<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Rules And Messages
    |--------------------------------------------------------------------------
    |
    */

    'product' => [
        'rules' => [
            'domain'                => 'required|in:tattoo,piercing',
            'category'              => 'required|exists:categories,id',
            'name'                  => 'required|max:300',
            'qty'                    => 'required|max:10',
            'discount'              => 'required|max:3',
            'base_price'            => 'required|max:15',
            'property_key'          => 'required',
            'property_value'        => 'required',
            'tags'                  => 'required',
            'short_description'     => 'required',
            'technical_description' => 'required',
            'primary'               => '',
            'catalog'               => '',
        ],
    ],
    'course' => [
        'rules' => [
            'domain'                => 'required|in:tattoo,piercing',
            'category'              => 'required|exists:categories,id',
            'artist'                => 'required|exists:artists,id',
            'title'                 => 'required|max:300',
            'subtitle'              => 'required|max:300',
            'duration'              => 'required|max:15',
            'tags'                  => 'required',
            'video_link'            => 'required',
            'short_description'     => 'required',
            'primary'               => '',
        ],
    ],
    'artist' => [
        'rules' => [
            'domain'    => 'required|in:tattoo,piercing',
            'role'      => 'required|in:author,artist',
            'name'      => 'required|max:300',
            'biography' => 'required|max:1000',
            'picture'   => '',
        ],
    ],

    'slider' => [
        'rules' => [
            'domain'    => 'required|in:tattoo,piercing',
            /*'title'     => 'required',*/
            'link'      => 'required',
            'image'     => '',
        ],
    ],

    'promo' => [
        'rules' => [
            'domain'    => 'required|in:tattoo,piercing',
            'link'      => 'required',
            'image'     => '',
        ],
    ],

    'shipping' => [
        'rules' => [
            'cost'      => 'required',
        ],
    ],

    'tax' => [
        'rules' => [
            'tax'      => 'required',
        ],
    ],

    'support' => [
        'rules' => [
            'support_number'   => 'required',
            'support_email'    => 'required',
            'availability'     => 'required',
            'channel_type'     => 'required',
            'channel_username' => 'required',
            'questions'        => 'required',
            'answers'          => 'required',
        ],
    ],

    'about' => [
        'rules' => [
            'description'      => 'required',
        ],
    ],

    'role' => [
        'rules' => [
            'name'         => 'required',
            'permissions'  => 'required',
        ],
    ],

    'permission' => [
        'rules' => [
            'name'      => 'required',
        ],
    ],

    'operator' => [
        'rules' => [
            'full_name'  => 'required',
            'email'      => 'required',
            'mobile'     => 'required',
            'roles'      => 'required',
        ],
    ],

    'category' => [
        'rules' => [
            'name'         => 'required',
            'parent_id'    => 'required',
            'type'         => 'required',
            'domain'       => 'required',
        ],
    ],
];
