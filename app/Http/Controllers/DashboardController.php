<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Course;
use App\Models\Guest;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){

        $from = Verta::createJalali(\verta()->now()->format('Y'),1,1)->formatGregorian('Y-m-d');
        $to = Verta::createJalali(\verta()->now()->format('Y'),12,30)->formatGregorian('Y-m-d');

        $total_orders_count = Order::query()->count();
        $pending_orders_count = Order::query()->where('status','pending')->count();
        $queued_orders_count = Order::query()->where('status','queued')->count();
        $sent_orders_count = Order::query()->where('status','sent')->count();
        $returned_orders_count = Order::query()->where('status','returned')->count();
        $unpaid_orders_count = Order::query()->where('status','unpaid')->count();
        $annual_orders = Order::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_orders as $key => $month){
            $annual_orders_chart_data[$key] = count($month);
        }


        $total_orders_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('payable');
        $total_orders_discount_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('discount');
        $total_returned_orders_amount  = Order::query()->where('status', 'returned')->sum('payable');
        $total_orders_tax_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('tax');
        $total_orders_shipping_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('shipping');
        $annual_paid_orders = Order::select('id', 'created_at')->whereIn('status',['pending','queued','sent'])->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_paid_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_paid_orders as $key => $month){
            $annual_paid_orders_chart_data[$key] = count($month);
        }
        $annual_returned_orders = Order::select('id', 'created_at')->where('status','returned')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_returned_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_returned_orders as $key => $month){
            $annual_returned_orders_chart_data[$key] = count($month);
        }


        $guest_customers_count  = Guest::query()->count();
        $member_customers_count = User::query()->count();
        $total_customers_count  = $guest_customers_count + $member_customers_count;
        $annual_member_customers = User::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_member_customers_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_member_customers as $key => $month){
            $annual_member_customers_chart_data[$key] = count($month);
        }
        $annual_guest_customers = Guest::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_guest_customers_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_guest_customers as $key => $month){
            $annual_guest_customers_chart_data[$key] = count($month);
        }


        $total_products_count  = Product::query()->count();
        $total_enable_products_count  = Product::query()->where('status','enable')->count();
        $total_disable_products_count  = Product::query()->where('status', 'disable')->count();
        $annual_products = Product::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_products_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_products as $key => $month){
            $annual_products_chart_data[$key] = count($month);
        }

        $total_courses_count  = Course::query()->count();
        $total_enable_courses_count  = Course::query()->where('status','enable')->count();
        $total_disable_courses_count  = Course::query()->where('status', 'disable')->count();
        $annual_courses = Course::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_courses_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_courses as $key => $month){
            $annual_courses_chart_data[$key] = count($month);
        }


        $total_artists_count  = Artist::query()->count();
        $total_artists_artist_count  = Artist::query()->where('role','artist')->count();
        $total_artists_author_count  = Artist::query()->where('role', 'author')->count();
        $annual_artists_author = Artist::select('id', 'created_at')->where('role', 'author')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_artists_author_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_artists_author as $key => $month){
            $annual_artists_author_chart_data[$key] = count($month);
        }
        $annual_artists_artist = Artist::select('id', 'created_at')->where('role', 'artist')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_artists_artist_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_artists_artist as $key => $month){
            $annual_artists_artist_chart_data[$key] = count($month);
        }


        return view('dashboard', [
            'body' => [
                'total_orders_count'                 => $total_orders_count,
                'pending_orders_count'               => $pending_orders_count,
                'queued_orders_count'                => $queued_orders_count,
                'sent_orders_count'                  => $sent_orders_count,
                'returned_orders_count'              => $returned_orders_count,
                'unpaid_orders_count'                => $unpaid_orders_count,
                'annual_orders_chart_data'           => json_encode(array_values($annual_orders_chart_data)),

                'total_orders_amount'                => $total_orders_amount,
                'total_orders_discount_amount'       => $total_orders_discount_amount,
                'total_returned_orders_amount'       => $total_returned_orders_amount,
                'total_orders_tax_amount'            => $total_orders_tax_amount,
                'total_orders_shipping_amount'       => $total_orders_shipping_amount,
                'annual_paid_orders_chart_data'      => json_encode(array_values($annual_paid_orders_chart_data)),
                'annual_returned_orders_chart_data'  => json_encode(array_values($annual_returned_orders_chart_data)),

                'total_customers_count'              => $total_customers_count,
                'guest_customers_count'              => $guest_customers_count,
                'member_customers_count'             => $member_customers_count,
                'annual_member_customers_chart_data' => json_encode(array_values($annual_member_customers_chart_data)),
                'annual_guest_customers_chart_data'  => json_encode(array_values($annual_guest_customers_chart_data)),

                'total_products_count'               => $total_products_count,
                'total_enable_products_count'        => $total_enable_products_count,
                'total_disable_products_count'       => $total_disable_products_count,
                'annual_products_chart_data'         => json_encode(array_values($annual_products_chart_data)),

                'total_courses_count'               => $total_courses_count,
                'total_enable_courses_count'        => $total_enable_courses_count,
                'total_disable_courses_count'       => $total_disable_courses_count,
                'annual_courses_chart_data'         => json_encode(array_values($annual_courses_chart_data)),

                'total_artists_count'               => $total_artists_count,
                'total_artists_author_count'        => $total_artists_author_count,
                'total_artists_artist_count'        => $total_artists_artist_count,
                'annual_artists_author_chart_data'  => json_encode(array_values($annual_artists_author_chart_data)),
                'annual_artists_artist_chart_data'  => json_encode(array_values($annual_artists_artist_chart_data)),
            ]
        ]);
    }
}
