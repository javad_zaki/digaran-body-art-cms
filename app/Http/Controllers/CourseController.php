<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Category;
use App\Models\Course;
use App\Traits\MultimediaTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CourseController extends Controller
{

    use MultimediaTrait;

    public function create(Request $request){

        try {
            $categories = Category::query()
            ->where('type', 'course')
            ->where('status', 'enable')
            ->whereNull('deleted_at')->get();

            $artists = Artist::query()
            ->where('role', 'author')
            ->where('status','enable')
            ->whereNull('deleted_at')->get();

            return view('course.create', ['body' => ['categories' => $categories, 'artists' => $artists]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $course = Course::find($request->id);
            $course->cover = $this->urlMaker($course->load('cover')->cover, 'single', 'course');
            $categories = Category::query()
            ->where('type', 'course')
            ->where('status', 'enable')
            ->whereNull('deleted_at')->get();
            $artists = Artist::query()
            ->where('role', 'author')
            ->where('status','enable')
            ->whereNull('deleted_at')->get();

            return view('course.update', ['body' => [
                'course' => $course,
                'categories' => $categories,
                'artists' => $artists
            ]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.course.rules'), __('validation'));

        try {


            foreach ($request->tags as $key => $item){
                $tags[] = $item;
            }

            $course = Course::updateOrCreate(
                ['id' => $request->id],
                [
                    'code'                   => 'CR-'.Carbon::now()->timestamp,
                    'category_id'            => $request->category,
                    'artist_id'              => $request->artist,
                    'title'                  => $request->title,
                    'subtitle'               => $request->subtitle,
                    'duration'               => $request->duration,
                    'short_description'      => $request->short_description,
                    'tags'                   => implode(",",$request->tags),
                    'video_link'             => $request->video_link,
                    'domain'                 => $request->domain,
                ]
            );


            if (!is_null($request->id) && !is_null($request->cover)) {
                if(!is_null($course->load('cover')->primary)){
                    $this->delete($course->load('cover')->primary->id, 'course');
                }
                $this->uploadImage($course, $request->cover, 'course', 'cover');
            }


            $request->session()->flash('message', __('general.done'));
            return redirect()->back();

        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }


    public function index(Request $request){
        try {
            $courses = Course::all();
            $total_courses_count  = Course::query()->count();
            $total_enable_courses_count  = Course::query()->where('status','enable')->count();
            $total_disable_courses_count  = Course::query()->where('status', 'disable')->count();


            return view('course.index', ['body' =>
                [
                    'courses'                      => $courses,
                    'total_courses_count'          => $total_courses_count,
                    'total_enable_courses_count'   => $total_enable_courses_count,
                    'total_disable_courses_count'  => $total_disable_courses_count,

                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function overview(Request $request){
        try {

            $course = Course::with(['category', 'artist'])->where('id', $request->id)->first();
            $course->cover = $this->urlMaker($course->load('cover')->cover, 'single', 'course');
            //$course->video = $this->urlMaker($course->load('video')->video, 'single', 'course');

            return view('course.overview', ['body' =>
                ['course' => $course]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $course = Course::find($request->id);
            $course->status = 'enable';
            $course->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $course = Course::find($request->id);
            $course->status = 'disable';
            $course->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function featured(Request $request){
        try {
            $course = Course::find($request->id);
            $course->type = 'featured';
            $course->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function general(Request $request){
        try {
            $course = Course::find($request->id);
            $course->type = 'general';
            $course->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
