<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class FinancialController extends Controller
{
    public function index(){

        $from = Verta::createJalali(\verta()->now()->format('Y'),1,1)->formatGregorian('Y-m-d');
        $to = Verta::createJalali(\verta()->now()->format('Y'),12,30)->formatGregorian('Y-m-d');

        $total_orders_count = Order::query()->count();
        $pending_orders_count = Order::query()->where('status','pending')->count();
        $queued_orders_count = Order::query()->where('status','queued')->count();
        $sent_orders_count = Order::query()->where('status','sent')->count();
        $returned_orders_count = Order::query()->where('status','returned')->count();
        $unpaid_orders_count = Order::query()->where('status','unpaid')->count();
        $annual_orders = Order::select('id', 'created_at')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_orders as $key => $month){
            $annual_orders_chart_data[$key] = count($month);
        }


        $total_orders_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('payable');
        $total_orders_discount_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('discount');
        $total_returned_orders_amount  = Order::query()->where('status', 'returned')->sum('payable');
        $total_orders_tax_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('tax');
        $total_orders_shipping_amount  = Order::query()->whereIn('status', ['pending','queued','sent'])->sum('shipping');
        $annual_paid_orders = Order::select('id', 'created_at')->whereIn('status',['pending','queued','sent'])->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_paid_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_paid_orders as $key => $month){
            $annual_paid_orders_chart_data[$key] = count($month);
        }
        $annual_returned_orders = Order::select('id', 'created_at')->where('status','returned')->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
            return \verta(Carbon::parse($date->created_at))->format('n');
        });
        $annual_returned_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
        foreach ($annual_returned_orders as $key => $month){
            $annual_returned_orders_chart_data[$key] = count($month);
        }

        return view('financial.index', [
            'body' => [
                'total_orders_count'                 => $total_orders_count,
                'pending_orders_count'               => $pending_orders_count,
                'queued_orders_count'                => $queued_orders_count,
                'sent_orders_count'                  => $sent_orders_count,
                'returned_orders_count'              => $returned_orders_count,
                'unpaid_orders_count'                => $unpaid_orders_count,
                'annual_orders_chart_data'           => json_encode(array_values($annual_orders_chart_data)),

                'total_orders_amount'                => $total_orders_amount,
                'total_orders_discount_amount'       => $total_orders_discount_amount,
                'total_returned_orders_amount'       => $total_returned_orders_amount,
                'total_orders_tax_amount'            => $total_orders_tax_amount,
                'total_orders_shipping_amount'       => $total_orders_shipping_amount,
                'annual_paid_orders_chart_data'      => json_encode(array_values($annual_paid_orders_chart_data)),
                'annual_returned_orders_chart_data'  => json_encode(array_values($annual_returned_orders_chart_data)),

            ]
        ]);
    }
}
