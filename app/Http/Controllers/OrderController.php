<?php

namespace App\Http\Controllers;

use App\Http\Traits\SMSTrait;
use App\Models\Guest;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use SMSTrait;

    public function index(Request $request){
        try {
            $orders = Order::all();
            $total_orders_count = Order::query()->count();
            $pending_orders_count = Order::query()->where('status','pending')->count();
            $queued_orders_count = Order::query()->where('status','queued')->count();
            $sent_orders_count = Order::query()->where('status','sent')->count();
            $returned_orders_count = Order::query()->where('status','returned')->count();
            $unpaid_orders_count = Order::query()->where('status','unpaid')->count();

            return view('order.index', ['body' =>
                [
                    'orders'                => $orders,
                    'total_orders_count'    => $total_orders_count,
                    'pending_orders_count'  => $pending_orders_count,
                    'queued_orders_count'   => $queued_orders_count,
                    'sent_orders_count'     => $sent_orders_count,
                    'returned_orders_count' => $returned_orders_count,
                    'unpaid_orders_count'   => $unpaid_orders_count,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function overview(Request $request){
        try {

            $order = Order::with('transaction')->where('id', $request->id)->first();
            if($order->user_type == 'member'){
                $user = User::find($order->user_id);
            }
            else{
                $user = Guest::query()->where('order_id', $order->id)->first();
            }

            return view('order.overview', ['body' =>
                ['order' => $order, 'user' => $user, 'items' => json_decode($order['items'], true)]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }


    public function status(Request $request){
        try {

            switch ($request->status){

                case "queued":
                    $status = "queued";
                    $farsi_status = "در حال بسته بندی";
                    break;

                case "sent":
                    $status = "sent";
                    $farsi_status = "ارسال شده";
                    break;

                case "returned":
                    $status = "returned";
                    $farsi_status = "مرجوع شده";
                    break;

                default:
                    $status = "pending";
                    $farsi_status = "در حال بررسی";
                    break;

            }


            $order = Order::query()->where('id', $request->id)->first();
            $order->status = $request->status;
            $order->save();

            if($order->user_type == 'member') {
                $user = User::query()->where('id', $order->user_id)->first();
                $receptor = $user->mobile;
            }
            else{
                $guest = $order->load('guest')->guest;
                $receptor = $guest->mobile;
            }

            $this->send($receptor, " فروشگاه دیگران - کاربر گرامی، وضعیت فعلی سفارش با کد {$order->code}  ، {$farsi_status} است. ");

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }
}
