<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Multimedia;
use App\Models\Product;
use App\Traits\MultimediaTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    use MultimediaTrait;

    public function create(Request $request){

        try {
            $categories = Category::query()->whereNull('deleted_at')->get();
            return view('product.create', ['body' => ['categories' => $categories]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $product = Product::find($request->id);
            $product->primary = $this->urlMaker($product->load('primary')->primary, 'single', 'product');
            $product->catalog = $this->urlMaker($product->load('catalog')->catalog, 'multiple', 'product');
            $categories = Category::query()->whereNull('deleted_at')->get();

            return view('product.update', ['body' => ['product' => $product, 'categories' => $categories]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.product.rules'), __('validation'));

        try {

            foreach ($request->property_key as $key => $item){
                $properties[] = ['key' => $item, 'value' => $request->property_value[$key]];
            }

            foreach ($request->tags as $key => $item){
                $tags[] = $item;
            }

            $product = Product::updateOrCreate(
                ['id' => $request->id],
                [
                    'category_id'            => $request->category,
                    'name'                   => $request->name,
                    'sku'                    => 'PR-'.Carbon::now()->timestamp,
                    'qty'                    => $request->qty,
                    'discount'               => $request->discount,
                    'base_price'             => $request->base_price,
                    'price_after_discount'   => $request->base_price * ((100-$request->discount) / 100),
                    'short_description'      => $request->short_description,
                    'technical_description'  => $request->technical_description,
                    'tags'                   => implode(",",$request->tags),
                    'properties'             => json_encode($properties),
                    'domain'                 => $request->domain,
                ]
            );

            if (!is_null($request->id) && !is_null($request->primary)) {
                if(!is_null($product->load('primary')->primary)){
                    $this->delete($product->load('primary')->primary->id, 'product');
                }
                $this->uploadImage($product, $request->primary, 'product');
            }

            if (!is_null($request->catalog)) {
                foreach ($request->catalog as $item){
                    $this->uploadImage($product, $item, 'product', 'catalog');
                }
            }

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();

        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function index(Request $request){
        try {
            $products = Product::all();
            $total_products_count  = Product::query()->count();
            $total_enable_products_count  = Product::query()->where('status','enable')->count();
            $total_disable_products_count  = Product::query()->where('status', 'disable')->count();

            return view('product.index', ['body' =>
                [
                    'products'                     => $products,
                    'total_products_count'         => $total_products_count,
                    'total_enable_products_count'  => $total_enable_products_count,
                    'total_disable_products_count' => $total_disable_products_count,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function overview(Request $request){
        try {

            $product = Product::with('category')->where('id', $request->id)->first();
            $product->primary = $this->urlMaker($product->load('primary')->primary, 'single', 'product');
            $product->catalog = $this->urlMaker($product->load('catalog')->catalog, 'multiple', 'product');

            return view('product.overview', ['body' =>
                ['product' => $product]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $product = Product::find($request->id);
            $product->status = 'enable';
            $product->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $product = Product::find($request->id);
            $product->status = 'disable';
            $product->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function deleteMultimedia(Request $request){
        try {
            $multimedia = Multimedia::find($request->id);
            $this->delete($request->id, 'product');
            $multimedia->delete();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function color(Request $request){
        try {
            $colors = Multimedia::query()
            ->where('multimedia_type', 'App\Models\Product')
            ->where('multimedia_id', $request->id)
            ->where('type', 'color')
            ->whereNull('deleted_at')
            ->get();
            $colors = $this->urlMaker($colors, 'multiple', 'product');
            return view('product.color', ['body' => ['colors' => $colors, 'id' => $request->id]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function storeColor(Request $request){
        try {
            $product = Product::find($request->id);
            if(!is_null($request->color)){
                $this->uploadImage($product, $request->color, 'product', 'color', json_encode(["color" => $request->color, "hex" => $request->hex]));
            }
            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }


    public function featured(Request $request){
        try {
            $product = Product::find($request->id);
            $product->type = 'featured';
            $product->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function general(Request $request){
        try {
            $product = Product::find($request->id);
            $product->type = 'general';
            $product->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
