<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Traits\MultimediaTrait;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    use MultimediaTrait;

    public function create(Request $request){

        try {
            return view('category.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $category = Category::find($request->id);
            $category->image = $this->urlMaker($category->load('image')->image, 'single', 'category');
            return view('category.update', ['body' => ['category' => $category]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.category.rules'), __('validation'));

        try {

            $category = Category::updateOrCreate(
                ['id' => $request->id],
                [
                    'parent_id' => $request->parent_id,
                    'name'      => $request->name,
                    'type'      => $request->type,
                    'domain'    => $request->domain,
                ]
            );

            if (!is_null($request->id) && !is_null($request->image) && !is_null($category->load('image')->image)) $this->delete($category->load('image')->image->id, 'category');

            if(!is_null($request->image)){
                $this->uploadImage($category, $request->image, 'category');
            }

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function index(Request $request){
        try {
            $categories = Category::query()->whereNull('deleted_at')->get();

            return view('category.index', ['body' =>
                [
                    'categories' => $categories,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $category = Category::find($request->id);
            $category->status = 'enable';
            $category->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $category = Category::find($request->id);
            $category->status = 'disable';
            $category->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }
}
