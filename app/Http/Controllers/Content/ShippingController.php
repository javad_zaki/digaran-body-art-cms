<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class ShippingController extends Controller
{

    public function index(Request $request){

        try {
            $shippings = Content::query()->where('type', 'shipping')->get();
            return view('content.shipping.index', ['body' => ['shippings' => $shippings]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){
        try {
            return view('content.shipping.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }


    public function update(Request $request){
        try {
            $shipping = Content::query()->where('type','shipping')->first();
            return view('content.shipping.update', ['body' => ['shipping' => $shipping]]);
       }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.shipping.rules'), __('validation'));

        try {
            $detail = json_encode([
                'cost' => $request->cost,
            ]);

            $shipping = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'shipping',
                    'detail'   => $detail,
                    'domain'   => 'general',
                ]
            );


            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function enable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'enable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'disable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }
}
