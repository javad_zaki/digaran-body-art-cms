<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class AboutController extends Controller
{

    public function index(Request $request){

        try {
            $abouts = Content::query()->where('type', 'about')->get();
            return view('content.about.index', ['body' => ['abouts' => $abouts]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){
        try {
            return view('content.about.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function update(Request $request){
        try {
            $about = Content::query()->where('type','about')->first();
            return view('content.about.update', ['body' => ['about' => $about]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.about.rules'), __('validation'));

        try {

            $detail = json_encode([
                'description' => $request->description,
            ]);

            $about = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'about',
                    'detail'   => $detail,
                    'domain'   => 'general',
                ]
            );

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();

        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function enable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'enable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'disable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }
}
