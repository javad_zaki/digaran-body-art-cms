<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class SupportController extends Controller
{

    public function index(Request $request){

        try {
            $supports = Content::query()->where('type', 'support')->get();
            return view('content.support.index', ['body' => ['supports' => $supports]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){
        try {
            return view('content.support.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function update(Request $request){
        try {
            $support = Content::query()->where('type','support')->first();
            return view('content.support.update', ['body' => ['support' => $support]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.support.rules'), __('validation'));

        try {

            foreach ($request->channel_type as $key => $channel){
                $channels[] = ['channel' => $channel, 'username' => $request->channel_username[$key]];
            }

            foreach ($request->questions as $key => $question){
                $faq[] = ['question' => $question, 'answer' => $request->answers[$key]];
            }

            $detail = json_encode([
                'channels' => $channels,
                'support_number' => $request->support_number,
                'support_email' => $request->support_email,
                'availability' => $request->availability,
                'faq' => $faq,
            ]);

            $support = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'support',
                    'detail'   => $detail,
                    'domain'   => 'general',
                ]
            );

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();

        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function enable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'enable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'disable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
