<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Traits\MultimediaTrait;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    use MultimediaTrait;

    public function create(Request $request){

        try {
            return view('content.promo.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $promo = Content::find($request->id);
            $promo->promoImage = $this->urlMaker($promo->load('promoImage')->promoImage, 'single', 'promo');

            return view('content.promo.update', ['body' => ['promo' => $promo]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.promo.rules'), __('validation'));

        try {
            $detail = json_encode(['link' => $request->link]);

            $promo = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'promo',
                    'detail'   => $detail,
                    'domain'   => $request->domain,
                ]
            );


            // make it helper
            if (!is_null($request->promo_image)) {
                if(!is_null($promo->load('promoImage')->promoImage)){
                    $this->delete($promo->load('promoImage')->promoImage->id, 'promo');
                }
                $this->uploadImage($promo, $request->promo_image, 'promo', 'promo');
            }


            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function index(Request $request){
        try {
            $promos = Content::query()->where('type', 'promo')->whereNull('deleted_at')->get();

            return view('content.promo.index', ['body' =>
                [
                    'promos' => $promos,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $promo = Content::find($request->id);
            $promo->status = 'enable';
            $promo->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $promo = Content::find($request->id);
            $promo->status = 'disable';
            $promo->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
