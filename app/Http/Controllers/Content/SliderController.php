<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Traits\MultimediaTrait;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use MultimediaTrait;

    public function create(Request $request){

        try {
            return view('content.slider.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->sliderImage = $this->urlMaker($slider->load('sliderImage')->sliderImage, 'single', 'slider');

            return view('content.slider.update', ['body' => ['slider' => $slider]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.slider.rules'), __('validation'));

        try {

            $detail = json_encode(['title' => 'بیشتر', 'link' => $request->link]);

            $slider = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'slider',
                    'detail'   => $detail,
                    'domain'   => $request->domain,
                ]
            );

            if (!is_null($request->slider_image)) {
                if(!is_null($slider->load('sliderImage')->sliderImage)){
                    $this->delete($slider->load('sliderImage')->sliderImage->id, 'slider');
                }
                $this->uploadImage($slider, $request->slider_image, 'slider', 'slider');
            }


            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function index(Request $request){
        try {
            $sliders = Content::query()->where('type', 'slider')->whereNull('deleted_at')->get();

            return view('content.slider.index', ['body' =>
                [
                    'sliders' => $sliders,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'enable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'disable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
