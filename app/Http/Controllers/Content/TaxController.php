<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;

class TaxController extends Controller
{

    public function index(Request $request){

        try {
            $taxs = Content::query()->where('type', 'tax')->get();
            return view('content.tax.index', ['body' => ['taxs' => $taxs]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){
        try {
            return view('content.tax.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }


    public function update(Request $request){
        try {
            $tax = Content::query()->where('type','tax')->first();
            return view('content.tax.update', ['body' => ['tax' => $tax]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.tax.rules'), __('validation'));

        try {

            $detail = json_encode([
                'tax' => $request->tax,
            ]);

            $tax = Content::updateOrCreate(
                ['id' => $request->id],
                [
                    'type'     => 'tax',
                    'detail'   => $detail,
                    'domain'   => 'general',
                ]
            );

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }


    public function enable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'enable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $slider = Content::find($request->id);
            $slider->status = 'disable';
            $slider->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
