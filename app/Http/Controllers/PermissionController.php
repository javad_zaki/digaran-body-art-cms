<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{

    public function index(Request $request){

        try {
            $permissions = Permission::all();
            return view('permission.index', ['body' => ['permissions' => $permissions]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){

        try {
            return view('permission.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){

        try {
            $permission = Permission::find($request->id);
            return view('permission.update', ['body' => ['permission' => $permission]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function store(Request $request){

        $this->validate($request, config('validation.permission.rules'), __('validation'));

        try {
            $permission = Permission::updateOrCreate(
                ['id' => $request->id],
                [
                    'name' => $request->name,
                ]
            );

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

}
