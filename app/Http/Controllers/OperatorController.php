<?php

namespace App\Http\Controllers;

use App\Models\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class OperatorController extends Controller
{

    public function index(Request $request){

        try {
            $operators = Operator::query()->where('mobile','!=', '09394682775')->get();

            return view('operator.index', ['body' =>
                [
                    'operators' => $operators,
                ]
            ]);
        } catch (\Exception $e) {
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){

        try {
            $roles = Role::query()->where('name','!=', 'super-admin')->get();
            return view('operator.create', ['body' => ['roles' => $roles]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){

        try {
            $operator = Operator::find($request->id);
            $operator_roles = $operator->load('roles')->roles;
            $selected_roles = [];
            foreach ($operator_roles as $role){
                $selected_roles [] = $role->name;
            }
            $operator->roles = $selected_roles;
            $roles = Role::query()->where('name','!=', 'super-admin')->get();

            return view('operator.update', ['body' => ['operator' => $operator, 'roles' => $roles]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function store(Request $request){

        $this->validate($request, config('validation.operator.rules'), __('validation'));

        try {
            $operator = Operator::updateOrCreate(
                ['id' => $request->id],
                [
                    'full_name' => $request->full_name,
                    'email'     => $request->email,
                    'mobile'    => $request->mobile,
                    'password'  => Hash::make('12345678'),
                ]
            );

            $operator->assignRole($request->roles);

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function enable(Request $request){
        try {
            $operator = Operator::find($request->operator_id);
            $operator->status = 'enable';
            $operator->save();
            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        } catch (\Exception $e) {
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $operator = Operator::find($request->operator_id);
            $operator->status = 'disable';
            $operator->save();
            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        } catch (\Exception $e) {
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
