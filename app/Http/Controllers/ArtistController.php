<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Traits\MultimediaTrait;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    use MultimediaTrait;

    public function create(Request $request){

        try {
            return view('artist.create');
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){
        try {
            $artist = Artist::find($request->id);
            $artist->load('multimedia')->multimedia;
            $artist->load('multimedia')->multimedia = $this->urlMaker($artist->load('multimedia')->multimedia, 'single','artist');

            return view('artist.update', ['body' => ['artist' => $artist]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function store(Request $request){

        $this->validate($request, config('validation.artist.rules'), __('validation'));

        try {

            foreach ($request->channel_username as $key => $channel){
                $channels[] = ['channel' => $request->channel_type[$key], 'username' => $channel];
            }

            $artist = Artist::updateOrCreate(
              ['id' => $request->id],
              [
                  'name' => $request->name,
                  'biography' => $request->biography,
                  'social' => json_encode($channels),
                  'role' => $request->role,
                  'domain' => $request->domain,
              ]
            );

            if (!is_null($request->id) && !is_null($request->picture)) $this->delete($request->id, 'artist');

            $this->uploadImage($artist, $request->picture, 'artist');

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();

        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function index(Request $request){
        try {
            $artists = Artist::all();
            $total_artists_count  = Artist::query()->count();
            $total_artists_artist_count  = Artist::query()->where('role','artist')->count();
            $total_artists_author_count  = Artist::query()->where('role', 'author')->count();

            return view('artist.index', ['body' =>
                [
                    'artists'                    => $artists,
                    'total_artists_count'        => $total_artists_count,
                    'total_artists_artist_count' => $total_artists_artist_count,
                    'total_artists_author_count' => $total_artists_author_count,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function profile(Request $request){
        try {

            $artist = Artist::find($request->id);
            $artist->load('multimedia')->multimedia;
            $artist->load('multimedia')->multimedia = $this->urlMaker($artist->load('multimedia')->multimedia, 'single','artist');

            return view('artist.profile', ['body' =>
                ['artist' => $artist]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $artist = Artist::find($request->id);
            $artist->status = 'enable';
            $artist->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $artist = Artist::find($request->id);
            $artist->status = 'disable';
            $artist->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

}
