<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Hekmatinasser\Verta\Facades\Verta;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request){
        try {
            $users = User::all();
            $guest_customers_count  = Guest::query()->count();
            $member_customers_count = User::query()->count();
            $total_customers_count  = $guest_customers_count + $member_customers_count;
            return view('user.index', ['body' =>
                [
                    'users' => $users,
                    'guest_customers_count' => $guest_customers_count,
                    'member_customers_count' => $member_customers_count,
                    'total_customers_count' => $total_customers_count,
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function profile(Request $request){
        try {
            $from = Verta::createJalali(\verta()->now()->format('Y'),1,1)->formatGregorian('Y-m-d');
            $to = Verta::createJalali(\verta()->now()->format('Y'),12,30)->formatGregorian('Y-m-d');

            $user = User::find($request->id);

            $annual_user_orders = Order::select('id', 'created_at')->where('user_id', $user->id)->whereIn('status',['pending','queued','sent'])->whereBetween('created_at', [$from,$to])->get()->groupBy(function ($date){
                return \verta(Carbon::parse($date->created_at))->format('n');
            });
            $annual_user_orders_chart_data = [1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0, 11 => 0, 12 => 0];
            foreach ($annual_user_orders as $key => $month){
                $annual_user_orders_chart_data[$key] = count($month);
            }

            $orders = Order::query()->where('user_id', $user->id)->get();

            return view('user.profile', ['body' =>
                [
                    'user' => $user,
                    'annual_user_orders_chart_data' => json_encode(array_values($annual_user_orders_chart_data)),
                    'orders' => $orders
                ]
            ]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function enable(Request $request){
        try {
            $user = User::find($request->id);
            $user->status = 'enable';
            $user->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }

    public function disable(Request $request){
        try {
            $user = User::find($request->id);
            $user->status = 'disable';
            $user->save();

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }
}
