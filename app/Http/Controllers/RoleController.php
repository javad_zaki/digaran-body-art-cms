<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{

    public function index(Request $request){

        try {
            $roles = Role::all();
            return view('role.index', ['body' => ['roles' => $roles]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function create(Request $request){

        try {
            $permissions = Permission::all();
            return view('role.create', ['body' => ['permissions' => $permissions]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function update(Request $request){

        try {
            $role = Role::find($request->id);
            $role_permissions = $role->load('permissions')->permissions;
            $selected_permissions = [];
            foreach ($role_permissions as $permission){
                $selected_permissions [] = $permission->name;
            }
            $role->permissions = $selected_permissions;
            $permissions = Permission::all();

            return view('role.update', ['body' => ['role' => $role, 'permissions' => $permissions]]);
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function store(Request $request){

        $this->validate($request, config('validation.role.rules'), __('validation'));

        try {

            $role = Role::updateOrCreate(
                ['id' => $request->id],
                [
                    'name' => $request->name,
                ]
            );

            $role->syncPermissions($request->permissions);

            $request->session()->flash('message', __('general.done'));
            return redirect()->back();
        }
        catch (\Exception $e){
            $request->session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }
}
