<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'id',
        'category_id',
        'name',
        'sku',
        'qty',
        'discount',
        'base_price',
        'price_after_discount',
        'short_description',
        'technical_description',
        'tags',
        'properties',
        'type',
        'status',
        'domain',
    ];

    /**
     * Get the product that owns the category.
     */
    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function primary()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type','=','image');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function catalog()
    {
        return $this->morphMany('App\Models\Multimedia', 'multimedia')->where('type','=','catalog');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function multimedia()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia');
    }
}
