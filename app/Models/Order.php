<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * Get the transaction that owns the order.
     */
    public function transaction(){
        return $this->hasOne('App\Models\Transaction');
    }

    /**
     * Get the transaction that owns the order.
     */
    public function guest(){
        return $this->hasOne('App\Models\Guest');
    }



}
