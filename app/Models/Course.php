<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'category_id',
        'artist_id',
        'title',
        'subtitle',
        'duration',
        'short_description',
        'tags',
        'video_link',
        'type',
        'status',
        'domain',
    ];

    /**
     * Get the course that owns the artist.
     */
    public function artist(){
        return $this->belongsTo('App\Models\Artist');
    }

    /**
     * Get the course that owns the category.
     */
    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function cover()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type','=','cover');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function video()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia')->where('type','=','video');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function multimedia()
    {
        return $this->morphOne('App\Models\Multimedia', 'multimedia');
    }
}
