<?php


namespace App\Traits;


use App\Models\Multimedia;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait MultimediaTrait
{


    /**
     * @param $entity
     * @param $file
     * @param $disk
     * @return bool|\Illuminate\Http\RedirectResponse|void
     */
    public function uploadImage($entity, $file, $disk, $type = 'image', $property = null){

        try {

            switch (json_decode($file)->type){
                case 'image/jpeg':
                    $format = '.jpeg';
                    break;
                case 'image/jpg':
                    $format = '.jpg';
                    break;
                case 'image/png':
                    $format = '.png';
                    break;
                case 'image/svg+xml':
                    $format = '.svg';
                    break;
                default:
                    return false;
                    break;
            }

            $original_file_name   = md5(mt_rand()).$format;
            $primary_file_name    = md5(mt_rand()).$format;
            $thumbnail_file_name  = md5(mt_rand()).$format;


            Storage::disk($disk)->put($original_file_name, base64_decode(json_decode($file)->data));

            if(json_decode($file)->size > 204800){

                Image::make(storage_path('app/public/'.$disk).'/'.$original_file_name)
                    ->resize(1000, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(storage_path('app/public/'.$disk).'/'.$primary_file_name);


                Image::make(storage_path('app/public/'.$disk).'/'.$original_file_name)
                    ->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(storage_path('app/public/'.$disk).'/'.$thumbnail_file_name);

                Storage::disk($disk)->delete($original_file_name);
            }
            else{
                $primary_file_name = $original_file_name;
                $thumbnail_file_name = $original_file_name;
            }


            $multimedia = new Multimedia();
            $multimedia->mime_type = json_decode($file)->type;
            $multimedia->primary_file_name = $primary_file_name;
            $multimedia->thumbnail_file_name = $thumbnail_file_name;
            $multimedia->type = $type;
            $multimedia->property = $property;
            $entity->multimedia()->save($multimedia);

            return;

        } catch (\Exception $e) {
            session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    /**
     * @param $entity
     * @param $file
     * @param $disk
     * @return bool|\Illuminate\Http\RedirectResponse|void
     */
    public function uploadVideo($entity, $file, $disk, $type = 'video', $property = null){

        try {

            switch (json_decode($file)->type){
                case 'video/mp4':
                    $format = '.mp4';
                    break;
                case 'video/mpeg':
                    $format = '.mpeg';
                    break;
                case 'video/ogg':
                    $format = '.ogg';
                    break;
                case 'video/webm':
                    $format = '.webm';
                    break;
                default:
                    return false;
                    break;
            }

            $original_file_name   = md5(mt_rand()).$format;

            Storage::disk($disk)->put($original_file_name, base64_decode(json_decode($file)->data));

            $multimedia = new Multimedia();
            $multimedia->mime_type = json_decode($file)->type;
            $multimedia->primary_file_name = $original_file_name;
            $multimedia->thumbnail_file_name = $original_file_name;
            $multimedia->type = $type;
            $multimedia->property = $property;
            $entity->multimedia()->save($multimedia);

            return;

        } catch (\Exception $e) {
            session()->flash('message', __('general.failed'));
            return redirect()->back();
        }

    }

    public function delete($id, $disk){
        try {
            $multimedia = Multimedia::find($id);

            Storage::disk($disk)->delete($multimedia->primary_file_name);
            Storage::disk($disk)->delete($multimedia->thumbnail_file_name);

            $multimedia->delete();

            return;
        }
        catch (\Exception $e){
            session()->flash('message', __('general.failed'));
            return redirect()->back();
        }
    }


    /**
     * @param $collection
     * @param $entity
     * @return mixed
     */
    public function urlMaker($collection, $type, $entity){

        switch ($type){

            case 'multiple' :

                if(!is_null($collection)){
                    foreach ($collection as $item) {
                        $item->primary_file_name = env('APP_URL')."/cdn/?entity={$entity}&file={$item->primary_file_name}";
                        $item->thumbnail_file_name = env('APP_URL')."/cdn/?entity={$entity}&file={$item->thumbnail_file_name}";
                    }
                }
            break;

            case 'single' :
                if(!is_null($collection)){
                    $collection->primary_file_name = env('APP_URL')."/cdn/?entity={$entity}&file={$collection->primary_file_name}";
                    $collection->thumbnail_file_name = env('APP_URL')."/cdn/?entity={$entity}&file={$collection->thumbnail_file_name}";
                }
            break;
        }

        return $collection;
    }


}
